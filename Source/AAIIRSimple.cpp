/*
 *	File:		AAIIRSimple.cpp
 *
 *	Version:	1.3
 *
 *	Created:	12-11-10 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AAIIRSimple.h"

using namespace AADsp;
using namespace AAFilter;


void    OnePole1::LP::calculateCoeffs()
{
    double X = exp(-TWOPI*aAfilterParams[paramID_Freq]);
    simA0 = 1. - X;
    simA1 = 0.;
    simB1 = -X;
}
 
void    OnePole1::HP::calculateCoeffs()
{
    double X = exp(-TWOPI*aAfilterParams[paramID_Freq]);
    simA0 = (1. + X) / 2.;
    simA1 = -(1. + X) / 2.;
    simB1 = -X;
}

void    OnePole2::LP::calculateCoeffs()
{
    double C = 2. - cos(TWOPI*aAfilterParams[paramID_Freq]);
    simA1 = 0.;
    simB1 = sqrt(C*C-1) - C;
    simA0 = 1. + simB1;
}

void    OnePole2::HP::calculateCoeffs()
{
    double C = 2. + cos(TWOPI*aAfilterParams[paramID_Freq]);
    simA1 = 0.;
    simB1 = C - sqrt(C*C-1);
    simA0 = 1. - simB1;
}