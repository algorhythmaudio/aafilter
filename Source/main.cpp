/*
 *	File:		main.cpp
 *	
 *	Test file for AAFilter class.
 *	See AAFilter.h for usage and documentation.
 *
 */

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cmath>
#include <string.h>
#include <time.h>
#include <assert.h>
#include "sndfile.h"

// AA libraries
#include "AADsp.h"
#include "AABasic.h"
#include "AAFilterLibrary.h"

#define VECTORSIZE 8192
#define numFreqInResponse 1024

#define INFILE "/Users/Rainland/Programming/Sounds/zelda_m.wav"
#define OUTFILE "/Users/Rainland/Desktop/filterout.wav"

int main (int argc, char * const argv[]) 
{
	using namespace AADsp;
	
	SNDFILE *in, *out;
	SF_INFO soundfileInfo;
	ChanMode channelMode;
	
	clock_t starttime, endtime;
    std::ofstream fout;                         // simple text file to hold frequency response data
	
	float *inBuffer;							// soundfile buffer
	float *outBuffer;							// output
	uint SR, readCount, writeCount, samplesToWrite;
	
	printf("AAFilter test---\n\n");
	
	// open soundfiles
	if (!(in = sf_open(INFILE, SFM_READ, &soundfileInfo))) {
		printf("Could not open %s\n", argv[1]);
		exit(-1);
	}
	
	// validate soundfile properties
	if (soundfileInfo.channels > 2 || soundfileInfo.channels < 1) {
		printf("Error: %s must be either a mono or stereo file.\n", argv[1]);
		sf_close(in);
		exit(-1);
	}
	
	if (!(out = sf_open(OUTFILE, SFM_WRITE, &soundfileInfo))) {
		printf("Could not open %s\n", argv[2]);
		exit(-1);
	}
	
	// -- initialize buffers and set up parameters --
	
	SR = soundfileInfo.samplerate;
	channelMode = soundfileInfo.channels == 1 ? CHAN_MONO : CHAN_STEREO;
    
    // create low-pass Butterworth filter
    AAFilter::IIRBiquad<Butterworth::LP> bwFilter(true);
    AAFilterParams bwParams;
    bwParams.set(paramID_Freq, 400.l);
    bwParams.set(paramID_Sr, SR);
	bwParams.set(paramID_Stages, 1);
	bwFilter.setNumParamSmoothingSamples(2048);
    bwFilter.setAllParams(bwParams);
    
    // create a parametric equalizer (param smoothing true by default)
    AAFilter::IIRBiquad<ParametricEQ> eqFilter;
    AAFilterParams eqParams;
    eqParams.set(paramID_Freq, 3611.l);
    eqParams.set(paramID_Q, 6.7l);
    eqParams.set(paramID_EqGain, -15.2l);
    eqParams.set(paramID_Sr, SR);
    eqFilter.setAllParams(eqParams);
    
    // create an RBJ high-shelf filter
    AAFilter::IIRBiquad<RBJ::LP> rbjFilter(true);
    AAFilterParams rbjParams;
    rbjParams.set(paramID_Freq, 100.l);
    rbjParams.set(paramID_Q, 2.l);
    rbjParams.set(paramID_Sr, SR);
    rbjParams.set(paramID_EqGain, 4.2);
	rbjFilter.setNumParamSmoothingSamples(1024);
    rbjFilter.setAllParams(rbjParams);
	
	// create a Linkwitz-Riley LP filter
	AAFilter::IIRBiquad<LinkwitzRiley::HP> lrFilter(false);
	AAFilterParams lrParams;
	lrParams.set(paramID_Freq, 3000.l);
	lrParams.set(paramID_Sr, SR);
	lrFilter.setAllParams(lrParams);
	
	// create simple LP filters
	AAFilterParams simpleParams;
	simpleParams.set(paramID_Freq, 280.l);
	simpleParams.set(paramID_Sr, SR);
	AAFilter::IIRSimple<OnePole1::LP> lpSimple1;
	lpSimple1.setAllParams(simpleParams);
	AAFilter::IIRSimple<OnePole2::HP> lpSimple2;
	lpSimple2.setAllParams(simpleParams);
	lpSimple2.setParam(paramID_Freq, 2683.4);
	
	
	// create a simple 15-order moving average filter
	AAFilter::FIRMAverage<15> mAvgFilter;
	
	// create an FIR design, using an impulse response of a specified order and sampling rate
	float impulseH[] = { 1., 1., 1., 0., 0., 0., 0., 0. };
	FrequencySampling::LP fsDesign(impulseH, 15, SR);
	AAFilter::FIRDesign<FrequencySampling::LP> firDesign1(fsDesign);
	
	// create FIR designs using windowing
	Window::Hamming::LP hammingWindow(1100, 48, SR);
	AAFilter::FIRDesign<Window::Hamming::LP> firDesign2(hammingWindow);
	Window::Sinc::LP sincWindow(420, 144, SR);
	AAFilter::FIRDesign<Window::Sinc::LP> firDesign3(sincWindow);
	
	AAFilter::FIRPolyphase<Window::Sinc::LP> polyphaseF(&sincWindow, 8);
	
	/*** *** *** *** *** *** *** *** *** ***
	 Switch filter type here,  *filter = ???
	 *** *** *** *** *** *** *** *** *** ***/
	AAFilter::AAFilterBase *filter = &polyphaseF;
    
    // get frequency response of the filter
    AAFreqResponse freqResponseTable[numFreqInResponse];
    filter->getFrequencyResponse(freqResponseTable, numFreqInResponse);
    
    // write data to text file, and convert normalized frequency to Hz and magnitude to dB
    fout.open("freqresponse.txt");
    for (int i = 0; i < numFreqInResponse; ++i) {
        fout << (freqResponseTable[i].frequency*SR)/TWOPI << "\t" << ampToDB(freqResponseTable[i].magnitude) << '\n';
    }
    fout.close();
    
    
	inBuffer = new float[VECTORSIZE*channelMode];
	outBuffer = new float[VECTORSIZE*2];
	
	// -- MAIN LOOP --
	
	printf("Processing...\n");
    
    //double freq = filter->getParam(paramID_Freq);
    //double q = filter->getParam(paramID_Q);
	
	starttime = clock();
	
	do {
		
		readCount = (int)sf_readf_float(in, inBuffer, VECTORSIZE);
		
		samplesToWrite = filter->process(inBuffer, outBuffer, readCount);
		
		writeCount = (int)sf_writef_float(out, outBuffer, samplesToWrite);
        
        // automate some parameters
        // make sure it's the right set of filter parameters and the right filter!
		//freq += 13.l;
		//filter->setParam(paramID_Freq, freq);
		
	} while (readCount);
	
	endtime = clock();
	std::cout << "Elapsed time: " << (endtime - starttime) / (double)CLOCKS_PER_SEC << " secs\n";
	std::cout << "Done." << std::endl;
	
	delete[] inBuffer;
	delete[] outBuffer;
	sf_close(in);
	sf_close(out);
	
    return 0;
}
