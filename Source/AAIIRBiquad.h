/*
 *	File:		AAIIRBiquad.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-10 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *	@usage:--
 *		AAFilter::IIRBiquad<Butterworth::type> bwFilter(smoothing);
 *			type is one of LP | HP | BP | BR
 *			smoothing is 'true' for parameter smoothing, or 'false' otherwise
 *
 *      AAFilter::IIRBiquad<ParametricEQ> equalizer;
 *          Arguments same as above, but this class has only one type: EQ
 *          NOTE: filter order may not be applicable in this class, as setting the Q is the primary way
 *				of affecting the rolloff
 *
 *		AAFilter::IIRBiquad<RBJ::type> rbjFilter;
 *			Arguments same as above, but this class supports additional types, including
 *			LP | HP | BP1 | BP2 | BR | AP | EQ | LSh | HSh
 *
 *		bwFilter.process<float>(input, output, samples);
 *			Apply filter to a block of audio samples with 32-bit floating-point precision.
 *		bwFilter.reset();
 *			Reset filter state to clear internal buffer after entire process on signal is finished or stopped.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAIIRBiquad_h_
#define _AAIIRBiquad_h_

#include "AAFilter.h"

namespace AADsp {
namespace AAFilter {
    
#pragma mark ____IIRBiquad filter
//////////////////////////////////////////////////////////////////////////////////////////////////
// IIRBiquad filter
//
// Second-order infinite impulse response filters implemented with biquad difference equation,
// Direct Form II
// Supports parameter smoothing/automation of frequency, Q/BW, Gain, EqGain
// Includes the following types:
//      - Butterworth LP, HP, BP, BR
//		- RBJ LP, HP, BP1, BP2, BR, AP, EQ, LSh, HSh
//		- Linkwitz-Riley LP, HP
//////////////////////////////////////////////////////////////////////////////////////////////////
template <class FilterType>
class IIRBiquad : public AAFilterBase, public AASmoothedFilter {
        
public:
	
	enum { biquadBaseOrder = 2 }; // biquad difference equation is order 2
        
    IIRBiquad     (const bool pSmoothing=true) : AASmoothedFilter(pSmoothing)
    {
		// allocate memory for filter state
		z = new float[static_cast<ushort>(kLimits_Max[paramID_Stages])*biquadBaseOrder];
		reset();
        CRTP_CAST(FilterType)->calculateCoeffs();
		
		DEBUG_REPORT_OBJECT_CREATED
		DEBUG_REPORT_OBJECT_PARAMETERS
    }
	
    ~IIRBiquad    ()
    {
		delete[] z;
	}
	
	// reset, clears parameter smoothing and resets filter state z to 0
	//-------------------------------------------------------------------------------------------
	void reset ()
	{
		numSmoothingSamplesRemaining = -1; // next call to setParameters should not invoke parameter smoothing
		aAfilterSmoothingIncr.clear();
		resetFilter(static_cast<ushort>(kLimits_Max[paramID_Stages])*biquadBaseOrder);
	}
	
	// getOrder
	// actual filter order is the base order of the difference equation * number of cascaded stages
    //-------------------------------------------------------------------------------------------
	ushort	getOrder	()	const	{ return biquadBaseOrder * static_cast<ushort>(aAfilterParams[paramID_Stages]); }
	
	// setParam
    //-------------------------------------------------------------------------------------------
    void	setParam	(const ushort paramId, double value)
    {
		assert(paramId >= 0 && paramId < aAfilter_numParams);
		
        if ((numSmoothingSamplesRemaining == -1 && aAfilterParamSmoothing) || !aAfilterParamSmoothing) {
            aAfilterParams.set(paramId, value);
            numSmoothingSamplesRemaining = 0;
            CRTP_CAST(FilterType)->calculateCoeffs();
        } else {
            prepareParamSmoothing(aAfilterParams, paramId, value);
        }
    }
	
    // setAllParams
	//-------------------------------------------------------------------------------------------
    void    setAllParams      (const AAFilterParams& params)
    {
        if ((numSmoothingSamplesRemaining == -1 && aAfilterParamSmoothing) || !aAfilterParamSmoothing) {
            aAfilterParams = params;
            numSmoothingSamplesRemaining = 0;
            CRTP_CAST(FilterType)->calculateCoeffs();
        } else {
            prepareParamSmoothing(aAfilterParams, params);
        }
    }
	
    // process, 32-bit
	//-------------------------------------------------------------------------------------------
    ushort	process		(float *input, float *output, const ushort inSamples)
    {
        if (aAfilterBypass)
            return 0;
        
        ushort  i, j,
				samplesToProcess = 0;
		ushort  order = biquadBaseOrder * static_cast<ushort>(aAfilterParams[paramID_Stages]);
		outSamplesProcessed = 0;
                
		samplesToProcess = processSmoothing<float>(input, output, inSamples, order);
		for (i = samplesToProcess; i < inSamples; ++i) {
			// cascade process sample
			for (j = 0; j < order; j+=biquadBaseOrder)
				processSample<float>(input[i], output[i], j);
			++outSamplesProcessed;
		}
		
		return outSamplesProcessed;
	}
	
#ifdef AAFILTER_USE_64bit
	// process, 64-bit
	//-------------------------------------------------------------------------------------------
    ushort	process		(double *input, double *output, const ushort inSamples)
    {
        if (aAfilterBypass)
            return 0;
        
        ushort  i, j,
				samplesToProcess = 0;
		ushort  order = biquadBaseOrder * static_cast<ushort>(aAfilterParams[paramID_Stages]);
		outSamplesProcessed = 0;
			
		samplesToProcess = processSmoothing<double>(input, output, inSamples, order);
		for (i = samplesToProcess; i < inSamples; ++i) {
			// cascade process sample
			for (j = 0; j < order; j+=biquadBaseOrder)
				processSample<double>(input[i], output[i], j);
			++outSamplesProcessed;
		}
		
		return outSamplesProcessed;
	}
#endif
    
protected:
    
	double  bqA0, bqA1, bqA2,
			bqB0, bqB1, bqB2;         // biquad filter kernel coefficients
        
private:
        
	// processSample, Direct Form II implementation
	//-------------------------------------------------------------------------------------------
	template <typename Type>
	inline void	processSample (const Type &sampIn, Type &sampOut, const ushort stage)
	{
		Type W;
            
		W = sampIn - bqB1*z[stage] - bqB2*z[stage+1] + denormalCurrent.ac();
		sampOut = (bqA0*W + bqA1*z[stage] + bqA2*z[stage+1]) * aAfilterParams[paramID_Gain];
            
		z[stage+1] = z[stage];
		z[stage] = W;
	}
	
	// processSmoothing, implements parameter smoothing
	//-------------------------------------------------------------------------------------------
	template <typename Type>
	ushort processSmoothing (Type *input, Type *output, ushort samples, const ushort order)
	{
		ushort	samplesToProcess = (numSmoothingSamplesRemaining <= samples ? numSmoothingSamplesRemaining : samples),
				i, j, k;
		
		for (i = 0; i < samplesToProcess; ++i, --numSmoothingSamplesRemaining) {
			// interpolate parameter change
			for (j = 0; j < aAfilter_numSmoothingParams; ++j)
				aAfilterParams[j] += aAfilterSmoothingIncr[j];
			
			CRTP_CAST(FilterType)->calculateCoeffs();
			
			// cascade process sample
			for (k = 0; k < order; k+=biquadBaseOrder)
				processSample<Type>(input[i], output[i], k);
			++outSamplesProcessed;
		}
		
		return samplesToProcess;
	}
	
	// calcFreqResponseMagnitudeAt
	//-------------------------------------------------------------------------------------------
	inline float calcFreqResponseMagnitudeAt (const float w)
	{
		float c1 = 2.f * cosf(w);
		float c2 = 2.f * cosf(2.f*w);
		float transferFunction_Numerator = bqA0*bqA0 + bqA1*bqA1 + bqA2*bqA2 + c1*(bqA0*bqA1 + bqA1*bqA2) + c2*bqA0*bqA2;
		float transferFunction_Denominator = 1.f + bqB1*bqB1 + bqB2*bqB2 + c1*(bqB1 + bqB1*bqB2) + c2*bqB2;
		return sqrtf(transferFunction_Numerator / transferFunction_Denominator) * aAfilterParams[paramID_Stages];
	}
    
};	// IIRBiquad
    
} } // AAFilter namespace // AADsp namespace


#endif // _AAIIRBiquad_h_

