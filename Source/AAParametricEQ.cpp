/*
 *	File:		AAParametricEQ.cpp
 *
 *	Version:	1.3
 *
 *	Created:	12-10-15 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AAParametricEQ.h"

using namespace AADsp;
using namespace AAFilter;


void	ParametricEQ::calculateCoeffs	()
{
    double g = tan(aAfilterParams[paramID_Bw]*M_PI) * aAfilterParams[paramID_EqGain];
	double D = 1.l + g / aAfilterParams[paramID_EqGain];
    bqA0 = (1.l + g * aAfilterParams[paramID_EqGain]) / D;
    bqA1 = (-2.l * cos(TWOPI*aAfilterParams[paramID_Freq])) / D;
    bqA2 = (1.l - g * aAfilterParams[paramID_EqGain]) / D;
    bqB1 = bqA1;
    bqB2 = (1.l - g / aAfilterParams[paramID_EqGain]) / D;
}

