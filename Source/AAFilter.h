/*
 *	File:		AAFilter.h
 *	
 *	Version:	1.3.352
 * 
 *	Created:	12-09-23 by Christian Floisand
 *	Updated:	13-01-31 by Christian Floisand
 *	
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * 
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	AAFilterBase is a superclass of all defined filters.  
 *	AASmoothedFilter implements parameter smoothing for automation of freq, Q/BW, Gain, and EqGain.  
 *	All filter types that support this feature need to inherit from AASmoothedFilter.
 *
 *	Filters implemented include:
 *	(see individual header files more more information)
 *		- IIR:
 *			- Butterworth filters (LP, HP, BP, BR)
 *			- Parametric EQ filter (equalizer)
 *			- RBJ filters (LP, HP, BP1, BP2, BR, AP, EQ, LSh, HSh)
 *			- Linkwitz-Riley filters (LP, HP)
 *		- FIR filters:
 *			- Moving average (LP only)
 *			- Frequency sampling design (LP & HP)
 *			- Windowing designs: Hamming, Sinc (LP & HP)
 *			- Polyphase implementation
 *
 *  Include AAFilterLibrary.h to get all filters.
 *
 *	Supports single channel processing. For multiple channels, de-interleave audio buffer and apply 
 *	filter to each channel before interleaving and writing buffer to output.
 *
 *	reset() must be called after processing on an audio stream is finished
 *
 *	Compiler defines:
 *		- AAFILTER_USE_64bit : define to enable 64-bit processing functions
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	AAFilterParams contains all parameter information for all filters in the AAFilterBase set.
 *
 *	Copy constructor is supported as well as the assignment operator.
 *
 *	@usage:--
 *		AAFilterParams bwParams;
 *      bwParams.set(paramID_Freq, 1200);
 *      prev = bwParams.get(paramID_Q);
 *      bwFilter.setAllParams(bwParams);
 *
 *		Use set/get functions to set/retrieve the value of a parameter using its paramID enum.  
 *		Avoid using the indexing operator to access parameter values.
 *
 *      Set frequency value in Hz.
 *      Set bandwidth value in Hz.
 *      Set gain and eq gain values in dB.
 *
 *		To set a single parameter in the filter, use the filters setParam method.
 *		iirFilter.setParam(paramID_Gain, 1.5);
 *		WARNING: if using both an AAFilterParams instance and setting params individually, be sure to 
 *			keep the AAFilterParams instance up-to-date.
 *
 *		reset() can be used to set parameter values to their default settings.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *  Acquring frequency response
 *  @usage:--
 *      AAFreqResponse freqResponse[numFreq];
 *			numFreq is the number of frequencies to calculate data for within the range 
 *			MIN_FREQUENCY_IN_RESPONSE (default = 10Hz) to Nyquist
 *      rbjFilter.getFrequencyResponse(freqResponse, numFreq);
 *          populates freqResponse (of size numFreq) with frequency response data where freqResponse[i].magnitude 
 *			is the amplitude gain of the filter at corresponding normalized frequency freqResponse[i].frequency
 *          NOTE: freqResponse.frequency is normalized frequency value and freqResponse.magnitude is amplitude gain value
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAFilter_h_
#define _AAFilter_h_

#include <cstdlib>
#include <memory.h>

// AA libraries
#include "AADsp.h"
#include "AABasic.h"
#include "AAFilterParams.h"
#include "AAIntrinsics.h"

//macros
#define CRTP_CAST(T)	static_cast<T*>(this)	// cast for curiously recurring template pattern

#ifdef DEBUG
#include <iostream>
#include <typeinfo>
#define DEBUG_REPORT_OBJECT_CREATED		std::cout << typeid(this).name() << std::endl;
#define DEBUG_REPORT_OBJECT_PARAMETERS	printParameters();
#else
#define DEBUG_REPORT_OBJECT_CREATED
#define DEBUG_REPORT_OBJECT_PARAMETERS
#endif


namespace AADsp {
namespace AAFilter {
	
#pragma mark ____AAFilterBase Declaration
////////////////////////////////////////////////////////////////////////
// AAFilterBase class
//
// AAFilter interface class for all AA filters
////////////////////////////////////////////////////////////////////////

class AAFilterBase {
	
public:
	
	virtual		~AAFilterBase	();
	
	void		setBypass		(const bool bypass);
	bool		getBypass		()				const		{ return aAfilterBypass; }
	double		getParam		(const ushort paramId)	const;
	AAFilterParams getAllParams	()				const		{ return aAfilterParams; }
	
	// for debugging
	void		printParameters	()				const;
	
	// returns 1 on success, 0 otherwise
	ushort		getFrequencyResponse			(AAFreqResponse* const freqResponseArray, const ushort numFrequencies);
	
	virtual void	reset		() = 0;
	virtual ushort	getOrder	()				const		{ return static_cast<ushort>(aAfilterParams[paramID_Stages]); }
	
	virtual void	setParam	(const ushort paramId, double value) = 0;
	virtual void	setAllParams(const AAFilterParams& params) = 0;
	
	// 32-bit & 64-bit block processing
	virtual ushort	process		(float *input, float *output, const ushort inSamples) = 0;
#ifdef AAFILTER_USE_64bit
	virtual ushort	process		(double *input, double *output, const ushort inSamples) = 0;
#endif
	
protected:
	
	AAFilterBase	();
	void			resetFilter	(const ushort length);
	virtual float	calcFreqResponseMagnitudeAt	(const float w) = 0;
	
	AAFilterParams	aAfilterParams;			// filter parameters
	float			*z;						// filter state/history of delayed samples
	ushort			outSamplesProcessed;	// number of output samples processed in block, returned by process() functions
	bool			aAfilterBypass;			// filter bypass

};	// AAFilterBase
	
	
#pragma mark ____AASmoothedFilter Declaration
////////////////////////////////////////////////////////////////////////////
// AASmoothedFilter class
//
// Filters that support parameter smoothing must inherit from this class
////////////////////////////////////////////////////////////////////////////

class AASmoothedFilter {
	
public:
	
	virtual ~AASmoothedFilter ();
	
	void	setNumParamSmoothingSamples	(const uint nSamples);
	uint	getNumParamSmoothingSamples ()	const		{ return numSmoothingSamples; }
	void	setParamSmoothing		(const bool pSmoothing);
	bool	getParamSmoothing		()	const			{ return aAfilterParamSmoothing; }
	
protected:
	
	AASmoothedFilter ();
	AASmoothedFilter (const bool pSmoothing);
	void	prepareParamSmoothing	(const AAFilterParams& filterParams, const AAFilterParams& newParams);
	void	prepareParamSmoothing	(const AAFilterParams& filterParams, const ushort id, double val);
	
	// smoothing-specific variables
	AAFilterParams		aAfilterSmoothingIncr;		// interpolating increment amount during parameter smoothing
	bool				aAfilterParamSmoothing;		// flags whether parameter smoothing should be in effect for parameter automation
	float				paramSmoothingFactor;		// scales parameter smoothing values
	uint				numSmoothingSamples;		// number of samples to perform parameter smoothing over
	short				numSmoothingSamplesRemaining;	// remaining samples in parameter smoothing
	
private:
	
	static const uint kNumSmoothingSamples_Default	= 1024;
	static const uint kNumSmoothingSamples_Min		= 100;
	static const uint kNumSmoothingSamples_Max		= 100000;
	
};	// AASmoothedFilter
	
	
} }	// AAFilter namespace // AADsp namespace


#endif	// _AAFilter_h_

