/*
 *	File:		AAFIRDesign.cpp
 *
 *	Version:	1.3
 *
 *	Created:	12-11-16 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AAFIRDesign.h"

using namespace AADsp;
using namespace AAFilter;


#pragma mark ____Frequency sampling methods

// Frequency sampling methods
//-------------------------------------------------------------------------------------------------------
//
FrequencySampling::LP::LP (const float *impulseResponseArray, const ushort fOrder, const float sRate)
{
    assert(fOrder > 0 && fOrder <= maxFilterOrder_Design);  // 1 <= N <= max (50)
    
    aAfilterParams[paramID_Stages] = fOrder;
    aAfilterParams[paramID_Sr] = sRate;
    filterOrderIsEven = (fOrder % 2 == 0 ? true : false);
    
    ushort numImpulses = static_cast<ushort>((fOrder / 2.) + 0.5);
    
    z = new float[fOrder+1];
    kernel = new double[fOrder+1];
    H = new float [numImpulses];
    
    for (int i = 0; i < numImpulses; ++i)
        H[i] = impulseResponseArray[i];
    
    calculateCoeffs();
    reset();
}

FrequencySampling::LP::~LP()
{
    delete[] z;
    delete[] kernel;
    delete[] H;
}

FrequencySampling::LP::LP (const LP &lp)
{
    aAfilterParams = lp.aAfilterParams;
    
    ushort filterOrder = aAfilterParams[paramID_Stages] + 1;
    z = new float[filterOrder];
    kernel = new double[filterOrder];
    memcpy(kernel, lp.kernel, sizeof(double)*filterOrder);
    
    calculateCoeffs();
    reset();
}

//-----------------------------------------------------------
// **
// TODO: these calculations need to be carefully checked with regard to the value of N and loop conditions!
// **
void	FrequencySampling::LP::calculateCoeffs ()
{
    ushort N = aAfilterParams[paramID_Stages];
    ushort num_impulses = static_cast<ushort>((N / 2) + 0.5);
    int loop_condition = static_cast<ushort>((N - 1) / 2);
    double coeff = 1. / N;
    
    double sumH;
    
    for (int k = 0; k <= loop_condition; ++k) {
        
        sumH = 0.;
        
        for (int i = 1; i <= num_impulses; ++i) {
            sumH += ( H[i] * cos( (TWOPI*i/N)*(k-((N-1)/2.)) ) );
        }
        
        kernel[k] = coeff * ( H[0] + 2.*sumH );
    }
    
    for (int k = 0; k <= loop_condition; ++k)
        kernel[N-k-1] = kernel[k];
}
//-----------------------------------------------------------
FrequencySampling::HP::HP (const float *impulseResponseArray, const ushort fOrder, const float sRate)
{
    assert(fOrder > 0 && fOrder <= maxFilterOrder_Design);  // 1 <= N <= max (50)
    
    aAfilterParams[paramID_Stages] = fOrder;
    aAfilterParams[paramID_Sr] = sRate;
    filterOrderIsEven = (fOrder % 2 == 0 ? true : false);
    
    ushort numImpulses = static_cast<ushort>((fOrder / 2.) + 0.5);
    
    z = new float[fOrder+1];
    kernel = new double[fOrder+1];
    H = new float [numImpulses];
    
    for (int i = 0; i < numImpulses; ++i)
        H[i] = impulseResponseArray[i];
    
    calculateCoeffs();
    reset();
}

FrequencySampling::HP::~HP()
{
    delete[] z;
    delete[] kernel;
    delete[] H;
}

FrequencySampling::HP::HP (const HP &hp)
{
    aAfilterParams = hp.aAfilterParams;
    
    ushort filterOrder = aAfilterParams[paramID_Stages] + 1;
    z = new float[filterOrder];
    kernel = new double[filterOrder];
    memcpy(kernel, hp.kernel, sizeof(double)*filterOrder);
    
    calculateCoeffs();
    reset();
}
//-----------------------------------------------------------
void	FrequencySampling::HP::calculateCoeffs ()
{
    ushort N = aAfilterParams[paramID_Stages] + 1;    // account for first term in difference equation (+1)
    ushort num_impulses = static_cast<ushort>((N / 2) + 0.5);
    int loop_condition = static_cast<ushort>((N - 1) / 2);
    double coeff = 1. / N;
    
    double sumH;
    
    for (int k = 0; k <= loop_condition; ++k) {
        
        sumH = 0.;
        
        for (int i = 1; i <= num_impulses; ++i) {
            sumH += ( H[i] * cos( (TWOPI*i/N)*(k-((N-1)/2.)) ) );
        }
        
        kernel[k] = coeff * ( H[0] + 2.*sumH );
    }
    
    for (int k = 0; k <= loop_condition; ++k)
        kernel[N-k-1] = kernel[k];
    
    // inverse every other coefficient to get high-pass
    for (int k = 1; k < N; k+=2)
        kernel[k] *= -1.;
}


#pragma mark ____Window design methods

// Window design methods
//-------------------------------------------------------------------------------------------------------
//

#pragma mark ____Hamming Window

// Hamming window
//-------------------------------------------------------------------------------------------------------
Window::Hamming::LP::LP (const double freq, const ushort fOrder, const float sRate)
{
    assert(fOrder % 2 == 0);    // must be even
    
    aAfilterParams.set(paramID_Sr, sRate);
    aAfilterParams.set(paramID_Freq, freq);
    aAfilterParams[paramID_Stages] = fOrder;
    filterOrderIsEven = (fOrder % 2 == 0 ? true : false);
    
    z = new float[fOrder+1];
    kernel = new double[fOrder+1];
    
    calculateCoeffs();
    reset();
}

Window::Hamming::LP::~LP ()
{
    delete[] z;
    delete[] kernel;
}

Window::Hamming::LP::LP (const LP &lp)
{
    aAfilterParams = lp.aAfilterParams;
    
    ushort filterOrder = aAfilterParams[paramID_Stages] + 1;
    z = new float[filterOrder];
    kernel = new double[filterOrder];
    memcpy(kernel, lp.kernel, sizeof(double)*filterOrder);
    
    calculateCoeffs();
    reset();
}
//-----------------------------------------------------------
void    Window::Hamming::LP::calculateCoeffs()
{
    ushort  k,
            N = aAfilterParams[paramID_Stages];
    
    double C, coeff;
    
    // calculate first half of filter kernel; avoid division by 0 at midpoint when k = N/2
    for (k = 0; k < (N/2); ++k) {
        C = k - (N/2.);
        coeff = sin( TWOPI*C*aAfilterParams[paramID_Freq] ) / ( M_PI*C );
        kernel[k] = coeff * ( 0.54 + 0.46*cos((TWOPI*C)/N) );
    }
    kernel[k++] = 2. * aAfilterParams[paramID_Freq];
    for (k = 0; k < (N/2); ++k) // filter kernel is symmetrical
        kernel[N-k] = kernel[k];
}
//-----------------------------------------------------------
Window::Hamming::HP::HP (const double freq, const ushort fOrder, const float sRate)
{
    assert(fOrder % 2 == 0);    // must be even
    
    aAfilterParams.set(paramID_Sr, sRate);
    aAfilterParams.set(paramID_Freq, freq);
    aAfilterParams[paramID_Stages] = fOrder;
    filterOrderIsEven = (fOrder % 2 == 0 ? true : false);
    
    z = new float[fOrder+1];
    kernel = new double[fOrder+1];
    
    calculateCoeffs();
    reset();
}

Window::Hamming::HP::~HP ()
{
    delete[] z;
    delete[] kernel;
}

Window::Hamming::HP::HP (const HP &hp)
{
    aAfilterParams = hp.aAfilterParams;
    
    ushort filterOrder = aAfilterParams[paramID_Stages] + 1;
    z = new float[filterOrder];
    kernel = new double[filterOrder];
    memcpy(kernel, hp.kernel, sizeof(double)*filterOrder);
    
    calculateCoeffs();
    reset();
}
//-----------------------------------------------------------
void    Window::Hamming::HP::calculateCoeffs()
{
    ushort  k,
            N = aAfilterParams[paramID_Stages];
    
    double C, coeff;
    
    // calculate first half of filter kernel; avoid division by 0 at midpoint when k = N/2
    for (k = 0; k < (N/2); ++k) {
        C = k - (N/2.);
        coeff = sin( TWOPI*C*aAfilterParams[paramID_Freq] ) / ( M_PI*C );
        kernel[k] = coeff * ( 0.54 + 0.46*cos((TWOPI*C)/N) );
    }
    kernel[k++] = 2. * aAfilterParams[paramID_Freq];
    for (k = 0; k < (N/2); ++k) // filter kernel is symmetrical
        kernel[N-k] = kernel[k];
    
    // inverse every other coefficient to get high-pass
    for (int k = 1; k <= N; k+=2)
        kernel[k] *= -1.;
}


#pragma mark ____Sinc Window

// Sinc window
//-------------------------------------------------------------------------------------------------------
Window::Sinc::LP::LP (const double freq, const ushort fOrder, const float sRate)
{
    assert(fOrder % 2 == 0);    // must be even
    
    aAfilterParams.set(paramID_Sr, sRate);
    aAfilterParams.set(paramID_Freq, freq);
    aAfilterParams[paramID_Stages] = fOrder;
    filterOrderIsEven = (fOrder % 2 == 0 ? true : false);
    
    z = new float[fOrder+1];
    kernel = new double[fOrder+1];
    
    calculateCoeffs();
    reset();
}

Window::Sinc::LP::~LP ()
{
    delete[] z;
    delete[] kernel;
}

Window::Sinc::LP::LP (const LP &lp)
{
    aAfilterParams = lp.aAfilterParams;
    
    ushort filterOrder = aAfilterParams[paramID_Stages] + 1;
    z = new float[filterOrder];
    kernel = new double[filterOrder];
    memcpy(kernel, lp.kernel, sizeof(double)*filterOrder);
    
    calculateCoeffs();
    reset();
}
//-----------------------------------------------------------
void    Window::Sinc::LP::calculateCoeffs()
{
    ushort  k,
            N = aAfilterParams[paramID_Stages];
    
    double C, coeff, sum = 0.;
    
    // calculate filter kernel in two halves, because division by 0 will occur when k = N/2
    for (k = 0; k < (N/2); ++k) {
        C = k - (N/2.);
        coeff = sin( TWOPI*aAfilterParams[paramID_Freq]*C ) / C;
        kernel[k] = coeff * ( 0.42 - 0.5*cos((TWOPI*k)/N) + 0.08*cos((2.*TWOPI*k)/N) );
    }
    kernel[k++] = TWOPI * aAfilterParams[paramID_Freq];
    for (k = 0; k < (N/2); ++k) // filter kernel is symmetrical
        kernel[N-k] = kernel[k];
    
    // normalize for DC unity gain
    for (k = 0; k <= N; ++k)
        sum += kernel[k];
    for (k = 0; k <= N; ++k)
        kernel[k] /= sum;
}
//-----------------------------------------------------------
Window::Sinc::HP::HP (const double freq, const ushort fOrder, const float sRate)
{
    assert(fOrder % 2 == 0);    // must be even
    
    aAfilterParams.set(paramID_Sr, sRate);
    aAfilterParams.set(paramID_Freq, freq);
    aAfilterParams[paramID_Stages] = fOrder;
    filterOrderIsEven = (fOrder % 2 == 0 ? true : false);
    
    z = new float[fOrder+1];
    kernel = new double[fOrder+1];
    
    calculateCoeffs();
    reset();
}

Window::Sinc::HP::~HP ()
{
    delete[] z;
    delete[] kernel;
}

Window::Sinc::HP::HP (const HP &hp)
{
    aAfilterParams = hp.aAfilterParams;
    
    ushort filterOrder = aAfilterParams[paramID_Stages] + 1;
    z = new float[filterOrder];
    kernel = new double[filterOrder];
    memcpy(kernel, hp.kernel, sizeof(double)*filterOrder);
    
    calculateCoeffs();
    reset();
}
//-----------------------------------------------------------
void    Window::Sinc::HP::calculateCoeffs()
{
    ushort  k,
            N = aAfilterParams[paramID_Stages];
    
    double C, coeff;
    
    // calculate filter kernel in two halves, because division by 0 will occur when k = N/2
    for (k = 0; k < (N/2); ++k) {
        C = k - (N/2.);
        coeff = sin( TWOPI*aAfilterParams[paramID_Freq]*C ) / C;
        kernel[k] = coeff * ( 0.42 - 0.5*cos((TWOPI*k)/N) + 0.08*cos((2.*TWOPI*k)/N) );
    }
    for (k = 0; k < (N/2); ++k)
        kernel[N-k] = kernel[k];
    
    // normalize for DC unity gain
    double sum = 0.;
    for (k = 0; k <= N; ++k)
        sum += kernel[k];
    for (k = 0; k <= N; ++k)
        kernel[k] /= sum;
    
    // inverse every other coefficient to get high-pass
    for (k = 1; k <= N; k+=2)
        kernel[k] *= -1.;
}

