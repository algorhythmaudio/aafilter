/*
 *	File:		AAFIRDesign.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-16 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *	@usage:--
 *		See below for usage instructions.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAFIRDesign_h_
#define _AAFIRDesign_h_

#include "AAFilter.h"

namespace AADsp {
namespace AAFilter {

#pragma mark ____FIRDesign filter
//////////////////////////////////////////////////////////////////////////////////////////////////
// FIRDesign filter
//
// FIR filter design class.
// Supports the design of non-recursive LTI filters through various methods, including frequency
// sampling & windowing.  See below for more specific design instructions.
// Parameter smoothing not supported.
// Includes the following types:
//      - LP and HP up to order 400
//////////////////////////////////////////////////////////////////////////////////////////////////
template <class DesignType>
class FIRDesign : public AAFilterBase {
	
public:
	
	enum { maxFilterOrder_Design = 400 };
	
	// constructor, destructor
	//-------------------------------------------------------------------------------------------
	FIRDesign (DesignType *fDesign) : filterDesign(fDesign)
	{
		DEBUG_REPORT_OBJECT_CREATED
		DEBUG_REPORT_OBJECT_PARAMETERS
	}
	
	~FIRDesign    ()
	{}
	
	// reset, resets filter state z to 0
	//-------------------------------------------------------------------------------------------
	void	reset ()
	{
		resetFilter(static_cast<ushort>(aAfilterParams[paramID_Stages]+1));
	}
	
	// setParam
	//-------------------------------------------------------------------------------------------
	void	setParam	(const ushort paramId, double value)
	{
		assert(paramId == paramID_Gain || paramId == paramID_Sr);
		aAfilterParams.set(paramId, value);
	}
	
	// setAllParams
	//-------------------------------------------------------------------------------------------
	void    setAllParams      (const AAFilterParams& params)
	{
		// cannot set all parameters in this filter type
		assert(0);
	}
	
	// process, 32-bit
	//-------------------------------------------------------------------------------------------
	ushort	process		(float *input, float *output, const ushort inSamples)
	{
		// buffers cannot be the same in this filter type due to accumulating process
		assert(input != output);
		if (aAfilterBypass)
			return 0;
		
		ushort  i, j,
				order = aAfilterParams[paramID_Stages],
				halfOrder = static_cast<ushort>(order / 2);
		float gain = aAfilterParams[paramID_Gain];
		outSamplesProcessed = 0;
		
		// convolution
		for (i = 0; i < inSamples; ++i) {
			z[0] = input[i];
			output[i] = 0.;
            
			// only need to sum half the kernel due to symmetry
			for (j = 0; j < halfOrder; ++j)
				output[i] += (kernel[j] * (z[j] + z[order-j]));
			if (filterOrderIsEven)	// include kernel midpoint if filter order is even
				output[i] += (kernel[j] * z[j]);
            
			for (j = order; j > 0; --j)
				z[j] = z[j-1];
			++outSamplesProcessed;
		}
		
		aA_SSE_vScale(output, gain, output, inSamples);
		
		return outSamplesProcessed;
	}
	
#ifdef AAFILTER_USE_64bit
	// process, 64-bit
	//-------------------------------------------------------------------------------------------
	ushort	process		(double *input, double *output, const ushort inSamples)
	{
		// buffers cannot be the same in this filter type due to accumulating process
		assert(input != output);
		if (aAfilterBypass)
			return 0;
		
		ushort  i, j,
				order = aAfilterParams[paramID_Stages],
				halfOrder = static_cast<ushort>(order / 2);
		double gain = aAfilterParams[paramID_Gain];
		outSamplesProcessed = 0;
		
		// convolution
		for (i = 0; i < inSamples; ++i) {
			z[0] = input[i];
			output[i] = 0.;
            
			// sum half of the kernel due to symmetry
			for (j = 0; j < halfOrder; ++j)
				output[i] += (kernel[j] * (z[j] + z[order-j]));
			if (filterOrderIsEven)	// include kernel midpoint if filter order is even
				output[i] += (kernel[j] * z[j]);
			output[i] *= gain;
            
			for (j = order; j > 0; --j)
				z[j] = z[j-1];
			++outSamplesProcessed;
		}
		
		return outSamplesProcessed;
	}
#endif
	
protected:
	
	FIRDesign () {}
	
	DesignType	*filterDesign;
	double		*kernel;
	bool		filterOrderIsEven;	// determines how to handle symmetry of filter kernel
	
private:
	
	// calcFreqResponseMagnitudeAt
	//-------------------------------------------------------------------------------------------
	inline float calcFreqResponseMagnitudeAt (const float w)
	{
		ushort order = aAfilterParams[paramID_Stages] + 1;
		float cosX = 0.f, sinX = 0.f;
		
		for (int i = 0; i < order; ++i) {
			cosX += (cosf(i*w)*kernel[i]);
			sinX += (sinf(i*w)*kernel[i]);
		}
		
		cosX *= cosX;	// cosX^2
		sinX *= sinX;	// sinX^2
		
		return sqrtf(cosX+sinX);
	}
	    
};	// FIRDesign
    
} } // AAFilter namespace // AADsp namespace


#pragma mark ____FIRDesign coefficient declarations
/////////////////////////////////////////////////////////////////////////////////////////////
// FIRDesign coefficient calcuations according to different design types.
//
// Design types include frequency sampling and windowing.
// Source: Dodge & Jerse, pg. 203-207
// LP & HP types
//
// Design basics for frequency sampling:
// The frequency design method works by taking an impulse response series that act as the desired frequency response
// of the filter from 0 -> nyquist.  Given the filter order (fOrder), the spacing of each impulse is calculated as SR/fOrder.
// In other words, the impulses given in impulseResponseArray will be spaced out evenly according to this factor.  The number
// of points taken from the impulseResponseArray is fOrder/2, rounded up, which puts the last sample just around nyquist.
// Higher order filters result in steeper curves, but at significanly increased computation time.
//
// Design basics for windowing:
// This method is a little more intuitive, as it uses a cutoff frequency (freq) and filter order (fOrder) to determine the
// frequency response.  The filter order must be an even number.  A higher order results in a steeper curve,
// but at a significantly increased computation time.
//
// To implement these designs, first declare a design instance:
//		e.g. FrequencySampling::LP fsDesign(IR, 18, 44100);
//			 Window::Hamming::LP hammingWindow(2000, 32, 48000);
// Then create the actual filter using a design instance as argument:
//		e.g. AAFilter::FIRDesign<FrequencySampling::LP> filter(fsDesign);
//			 AAFilter::FIRDesign<Window::Hamming::LP> filter(hammingWindow);
//
// Important note regarding LP/HP types:
// The high-pass filters in these designs take advantage of the fact that a non-recursive LP filter can be transformed into a HP
// filter by switching the polarity of every other coefficient starting with the second one.  The result is that the frequency given
// is NOT an absolute, but relative to Nyquist.  That is, with a frequency value of 2400 Hz given as a HP type for the windowing filter
// design, for example, the high-pass cutoff will be 2400 Hz less than Nyquist.  The similar principle applies to the
// frequency sampling method, given the impulse response array.
/////////////////////////////////////////////////////////////////////////////////////////////

namespace FrequencySampling {
	
	class LP : public AADsp::AAFilter::FIRDesign<LP> {
	public:
		LP (const float *impulseResponseArray, const ushort fOrder=1, const float sRate=AADsp::DEFAULT_SAMPLE_RATE);
		~LP ();
		LP (const LP &lp);
		
		void calculateCoeffs ();
		void getCoeffs (double *dest, const ushort size)
		{
			memcpy(dest, kernel, size);		// size = sizeof()*length
		}
	private:
		float *H;	// impulse response array, H(i)
	};
	
	class HP : public AADsp::AAFilter::FIRDesign<HP> {
	public:
		HP (const float *impulseResponseArray, const ushort fOrder=1, const float sRate=AADsp::DEFAULT_SAMPLE_RATE);
		~HP ();
		HP (const HP &hp);
		
		void calculateCoeffs ();
		void getCoeffs (double *dest, const ushort size)
		{
			memcpy(dest, kernel, size);
		}
	private:
		float *H;
	};
	
} // FrequencySampling namespace


namespace Window {
	
	namespace Hamming {
	
		class LP : public AADsp::AAFilter::FIRDesign<LP> {
		public:
			LP (const double freq, const ushort fOrder=1, const float sRate=AADsp::DEFAULT_SAMPLE_RATE);
			~LP ();
			LP (const LP &lp);
		
			void calculateCoeffs ();
			void getCoeffs (double *dest, const ushort size)
			{
				memcpy(dest, kernel, size);
			}
		};
		
		class HP : public AADsp::AAFilter::FIRDesign<HP> {
		public:
			HP (const double freq, const ushort fOrder=1, const float sRate=AADsp::DEFAULT_SAMPLE_RATE);
			~HP ();
			HP (const HP &hp);
		
			void calculateCoeffs ();
			void getCoeffs (double *dest, const ushort size)
			{
				memcpy(dest, kernel, size);
			}
		};
	
	} // Hamming namespace

	namespace Sinc {
	
		class LP : public AADsp::AAFilter::FIRDesign<LP> {
		public:
			LP (const double freq, const ushort fOrder=1, const float sRate=AADsp::DEFAULT_SAMPLE_RATE);
			~LP ();
			LP (const LP &lp);
		
			void calculateCoeffs ();
			void getCoeffs (double *dest, const ushort size)
			{
				memcpy(dest, kernel, size);
			}
		};
	
		class HP : public AADsp::AAFilter::FIRDesign<HP> {
		public:
			HP (const double freq, const ushort fOrder=1, const float sRate=AADsp::DEFAULT_SAMPLE_RATE);
			~HP ();
			HP (const HP &hp);
		
			void calculateCoeffs ();
			void getCoeffs (double *dest, const ushort size)
			{
				memcpy(dest, kernel, size);
			}
		};
	
	} // Sinc namespace
	
} // Window namespace


#endif // _AAFIRDesign_h_

