/*
 *	File:		AAFIRPolyphase.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-21 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *	TODO:	- optimize further to take advantage of symmetrical filter kernel
 *			- need to compensate for offset due to delay in the interpolator
 *			- possibly handle overlapping of processing blocks
 */

#ifndef _AAFIRPolyphase_h_
#define _AAFIRPolyphase_h_

#include "AAFilter.h"
#include "AAFIRDesign.h"


namespace AADsp {
namespace AAFilter {
        
#pragma mark ____FIRPolyphase filter
//////////////////////////////////////////////////////////////////////////////////////////////////
// FIRPolyphase filter
//
//////////////////////////////////////////////////////////////////////////////////////////////////
template <class DesignType>
class FIRPolyphase : public FIRDesign<DesignType> {
            
public:
	
	FIRPolyphase (DesignType *pFilter, const ushort nSubFilters) : prototypeFilter(pFilter)
	{
		ushort prototypeFilterOrder = static_cast<ushort>(prototypeFilter->getParam(paramID_Stages));
		numSubFilters = nSubFilters;
		
		// prototype filter's order must be a multiple of numSubFilters
		assert(prototypeFilterOrder % numSubFilters == 0);
		
		// the order of each sub-filter branch; since the number of delay elements and coefficients is 1 more
		// than the order in the prototype filter, we need to make room for the extra kernel coefficient;
		// the remaining coefficients in the sub-filter kernels will be 0
		subFilterOrder = prototypeFilterOrder / numSubFilters + 1;
		this->aAfilterParams = prototypeFilter->getAllParams();
		totalPolyphaseOrder = subFilterOrder * numSubFilters;
		
		z = new double[totalPolyphaseOrder];
		kernel = new double[totalPolyphaseOrder];
		subFilter = new double[numSubFilters];
		
		memset(kernel, 0, sizeof(double)*totalPolyphaseOrder);
		prototypeFilter->getCoeffs(kernel, sizeof(double)*(prototypeFilterOrder+1));
		
		reset();
	}
	
	~FIRPolyphase    ()
	{
		delete[] z;
		delete[] kernel;
		delete[] subFilter;
	}
	
	FIRPolyphase (const FIRPolyphase &filter)
	{
		numSubFilters = filter.numSubFilters;
		subFilterOrder = filter.subFilterOrder;
		
		z = new double[totalPolyphaseOrder];
		kernel = new double[totalPolyphaseOrder];
		subFilter = new double[numSubFilters];
		memcpy(kernel, filter.kernel, sizeof(double)*totalPolyphaseOrder);
	}
            
	// reset, resets filter state z to 0; kernel length = numSubFitlers * subFilterOrder
	//-------------------------------------------------------------------------------------------
	void	reset ()
	{
		memset(z, 0, sizeof(double)*totalPolyphaseOrder);
	}
            
	// setParam
	//-------------------------------------------------------------------------------------------
	void	setParam	(const ushort paramId, double value)
	{
		assert(paramId == paramID_Gain || paramId == paramID_Sr);
		this->aAfilterParams.set(paramId, value);
	}
            
	// setAllParams
	//-------------------------------------------------------------------------------------------
	void    setAllParams      (const AAFilterParams& params)
	{
		// cannot set all parameters in this filter type
		assert(0);
	}
            
	// process, 32-bit
	//-------------------------------------------------------------------------------------------
	ushort	process		(float *input, float *output, const ushort inSamples)
	{
		// buffers cannot be the same in this filter type due to accumulating process
		assert(input != output);
		if (prototypeFilter->getBypass())
			return 0;
		
		ushort i, j, k, n;
		float gain = this->aAfilterParams[paramID_Gain];
		outSamplesProcessed = 0;
		
		for (i = 0; i < inSamples; ++i) {
			z[0] = input[i];
			
			for (j = 0; j < numSubFilters; ++j) {
				subFilter[j] = 0.;
				for (k = 0; k < totalPolyphaseOrder; k+=numSubFilters) {
					subFilter[j] += (kernel[j+k] * z[j+k]);
				}
			}
			
			output[i] = subFilter[0];
			for (n = 1; n < numSubFilters; ++n)
				output[i] += subFilter[n];
			
			for (n = totalPolyphaseOrder-1; n > 0; --n)
				z[n] = z[n-1];
			++outSamplesProcessed;
		}
		
		aA_SSE_vScale(output, gain, output, inSamples);
		
		return outSamplesProcessed;
	}
	
#ifdef AAFILTER_USE_64bit
	// process, 64-bit
	//-------------------------------------------------------------------------------------------
	ushort	process		(double *input, double *output, const ushort inSamples)
	{
		// buffers cannot be the same in this filter type due to accumulating process
		assert(input != output);
		if (prototypeFilter->getBypass())
			return 0;
		
		ushort i, j, k, n;
		double gain = this->getParam(paramID_Gain);
		outSamplesProcessed = 0;
		
		for (i = 0; i < inSamples; ++i) {
			z[0] = input[i];
			
			for (j = 0; j < numSubFilters; ++j) {
				subFilter[j] = 0.;
				for (k = 0; k < totalPolyphaseOrder; k+=numSubFilters) {
					subFilter[j] += (kernel[j+k] * z[j+k]);
				}
			}
			
			output[i] = subFilter[0];
			for (n = 1; n < numSubFilters; ++n)
				output[i] += subFilter[n];
			output[i] *= gain;
			
			for (n = totalPolyphaseOrder-1; n > 0; --n)
				z[n] = z[n-1];
			++outSamplesProcessed;
		}
		
		return outSamplesProcessed;
	}
#endif
	
	// interpolate / upsampler
	// outStride is the stride length to parse the output buffer, equal to the upsampling factor L
	// inputSamples is the length of input; output is a factor of L longer than input
	//-------------------------------------------------------------------------------------------
	template <typename Type>
	ushort	interpolate		(Type *input, Type *output, const ushort outStride, const ushort inputSamples)
	{
		// buffers cannot be the same in this filter type due to accumulating process
		assert(input != output);
		
		ushort i, j, k, n, L;
		outSamplesProcessed = 0;
		Type gain = this->aAfilterParams[paramID_Gain];
		
		for (i = 0; i < inputSamples; ++i) {
			z[0] = input[i];
			
			for (j = 0; j < numSubFilters; ++j) {
				subFilter[j] = 0.;
				for (k = totalPolyphaseOrder, L = 0; k > 0; k-=numSubFilters, ++L) {
					subFilter[j] += z[j+L] * kernel[k-j-1];	// flipped kernel
				}
			}
			
			// output selector: x(n) -> y(Ln+L) (L=outStride)
			for (n = 0; n < numSubFilters; ++n) {
				output[i*outStride+n] = (subFilter[numSubFilters-n-1]*gain);
				++outSamplesProcessed;
			}
			
			for (n = subFilterOrder-1; n > 0; --n)
				z[n] = z[n-1];
		}
		
		return outSamplesProcessed;
	}
	
	// decimate / downsampler
	// inStride is the stride length to parse the input buffer, equal to the decimation factor M
	// outputSamples is the length of output; input is a factor of M shorter than output
	//-------------------------------------------------------------------------------------------
	template <typename Type>
	ushort	decimate		(Type *input, Type *output, const ushort inStride, const ushort outputSamples)
	{
		// buffers cannot be the same in this filter type due to accumulating process
		assert(input != output);
		
		ushort i, j, k, n, M;
		outSamplesProcessed = 0;
		
		for (i = 0, M = 0; i < outputSamples; ++i, M+=inStride) {
			
			for (j = 0; j < numSubFilters; ++j) {
				subFilter[j] = 0.;
				for (k = 0; k < totalPolyphaseOrder; k+=numSubFilters) {
					// input selector: x(Mn) -> y(n)
					subFilter[j] += input[M+j] * kernel[j+k];
				}
			}
			
			output[i] = subFilter[0];
			for (n = 1; n < numSubFilters; ++n)
				output[i] += subFilter[n];
			++outSamplesProcessed;
		}
		
		return outSamplesProcessed;
	}
            
protected:
            
	FIRPolyphase () {}
	
	DesignType		*prototypeFilter;
	double			*z, *kernel;
	double			*subFilter;				// sub-filter branches that make up the polyphase filter
	ushort			numSubFilters,
					subFilterOrder,
					totalPolyphaseOrder;	// = numSubFilters * subFilterOrder
	ushort			outSamplesProcessed;
            
private:
            
	// calcFreqResponseMagnitudeAt
	//-------------------------------------------------------------------------------------------
	inline float calcFreqResponseMagnitudeAt (const float w)
	{
		// NYI
		return 0.;
	}
            
};	// FIRPolyphase
        
} } // AAFilter namespace // AADsp namespace


#endif // _AAFIRPolyphase_h_

