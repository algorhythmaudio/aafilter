/*
 *	File:		AAIIRSimple.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-10 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *	@usage:--
 *		AAFilter::IIRSimple<DJBasic::type> lpFilter;
 *			type is one of LP | HP
 *
 *		AAFilter::IIRSimple<Simple::type> hpFilter;
 *			type is one of LP | HP
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAIIRFilter_h_
#define _AAIIRFilter_h_

#include "AAFilter.h"

namespace AADsp {
namespace AAFilter {

#pragma mark ____IIRSimple filter
//////////////////////////////////////////////////////////////////////////////////////////////////
// IIRSimple filter
//
// Small footprint, fast, and lightweight simple first-order infinite impulse response filters.
// Parameter smoothing not supported.
// Includes the following types:
//      - LP, HP
//////////////////////////////////////////////////////////////////////////////////////////////////
template <class FilterType>
class IIRSimple : public AAFilterBase {

public:
	
    IIRSimple     () : AAFilterBase()
    {
		// allocate memory for filter state
		z = new float[static_cast<ushort>(kLimits_Max[paramID_Stages])];
		reset();
        CRTP_CAST(FilterType)->calculateCoeffs();
		
		DEBUG_REPORT_OBJECT_CREATED
		DEBUG_REPORT_OBJECT_PARAMETERS
    }
	
    ~IIRSimple    ()
    {
		delete[] z;
	}
	
	// reset, resets filter state z to 0
	//-------------------------------------------------------------------------------------------
	void reset ()
	{
		resetFilter(static_cast<ushort>(kLimits_Max[paramID_Stages]));
	}
	
    // setParam
    //-------------------------------------------------------------------------------------------
    void	setParam	(const ushort paramId, double value)
    {
        assert(paramId >= 0 && paramId < aAfilter_numParams);
		
		aAfilterParams.set(paramId, value);
		CRTP_CAST(FilterType)->calculateCoeffs();
    }
	
    // setAllParams
    //-------------------------------------------------------------------------------------------
    void    setAllParams      (const AAFilterParams& params)
    {
		aAfilterParams = params;
		CRTP_CAST(FilterType)->calculateCoeffs();
    }
	
    // process, 32-bit
    //-------------------------------------------------------------------------------------------
    ushort	process		(float *input, float *output, const ushort inSamples)
    {
        if (aAfilterBypass)
            return 0;
		
        ushort  i, j,
				order = static_cast<ushort>(aAfilterParams[paramID_Stages]);
		outSamplesProcessed = 0;
		
        for (i = 0; i < inSamples; ++i) {
            // cascade process sample
            for (j = 0; j < order; ++j)
                processSample<float>(input[i], output[i], j);
			++outSamplesProcessed;
        }
		
		return outSamplesProcessed;
    }
	
#ifdef AAFILTER_USE_64bit
    // process, 64-bit
    //-------------------------------------------------------------------------------------------
    ushort	process		(double *input, double *output, const ushort inSamples)
    {
        if (aAfilterBypass)
            return 0;
		
        ushort  i, j,
				order = static_cast<ushort>(aAfilterParams[paramID_Stages]);
		outSamplesProcessed = 0;
		
        for (i = 0; i < inSamples; ++i) {
            // cascade process sample
            for (j = 0; j < order; ++j)
                processSample<double>(input[i], output[i], j);
			++outSamplesProcessed;
        }
		
		return outSamplesProcessed;
    }
#endif
    
protected:
	
    double  simA0, simA1, simB1;	// first-order kernel coefficients
	
private:
        
    // processSample, Direct Form II first-order recursive difference equation
    //-------------------------------------------------------------------------------------------
    template <typename Type>
    inline void	processSample (const Type &sampIn, Type &sampOut, const ushort stage)
    {
		Type W;
		
        W = sampIn - simB1*z[stage] + denormalCurrent.ac();
        sampOut = (simA0*W + simA1*z[stage]) * aAfilterParams[paramID_Gain];
		
        z[stage] = W;
    }
        
	// calcFreqResponseMagnitudeAt
	//-------------------------------------------------------------------------------------------
	inline float calcFreqResponseMagnitudeAt (const float w)
	{
		float c1 = 2.f * cosf(w);
		float transferFunction_Numerator = simA0*simA0 + simA1*simA1 + c1*simA0*simA1;
		float transferFunction_Denominator = 1.f + simB1*simB1 + c1*simB1;
		return sqrtf(transferFunction_Numerator / transferFunction_Denominator) * aAfilterParams[paramID_Stages];
	}
        
};	// IIRSimple
    
} } // AAFilter namespace // AADsp namespace


#pragma mark ____IIRSimple coefficient declarations
//////////////////////////////////////////////////////////////////
// Simple IIR coefficient subclass declarations, including the
// basic recursive filter designs from Dodge & Jerse
//
// Source: Smith, DSP Guide, ch. 19 (OnePole1)
// Source: Dodge & Jerse, pg. 210 (OnePole2)
// LP, HP types
//////////////////////////////////////////////////////////////////

namespace OnePole1 {

	class LP : public AADsp::AAFilter::IIRSimple<LP> {
	public:
		void calculateCoeffs ();
	};
 
	class HP : public AADsp::AAFilter::IIRSimple<HP> {
	public:
		void calculateCoeffs ();
	};
	
} // OnePole1 namespace

namespace OnePole2 {
	
	class LP : public AADsp::AAFilter::IIRSimple<LP> {
	public:
		void calculateCoeffs ();
	};
	
	class HP : public AADsp::AAFilter::IIRSimple<HP> {
	public:
		void calculateCoeffs ();
	};
	
} // OnePole2 namespace

#endif // _AAIIRSimple_h_

