/*
 *	File:		AAParametricEQ.h
 *
 *	Version:	1.3
 *
 *	Created:	12-10-15 by Christian Floisand
 *	Updated:	12-11-08 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AAParametricEQ_h_
#define _AAParametricEQ_h_

#include "AAIIRBiquad.h"


/////////////////////////////////////////////////////////////////////////////////////
// ParametricEQ coefficient subclass declarations, to be used with IIRBiquad filters
//
// EQ type only
// source: RBJ EQ Cookbook
/////////////////////////////////////////////////////////////////////////////////////
class ParametricEQ : public AADsp::AAFilter::IIRBiquad<ParametricEQ> {
public:
    void calculateCoeffs ();
}; // AAParametricEQ


#endif // _AAParametricEQ_h_
