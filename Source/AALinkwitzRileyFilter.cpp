/*
 *	File:		AALinkwitzRileyFilter.cpp
 *
 *	Version:	1.3
 *
 *	Created:	13-01-05 by Christian Floisand
 *	Updated:	13-01-05 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AALinkwitzRileyFilter.h"

using namespace AADsp;
using namespace AAFilter;


void	LinkwitzRiley::LP::calculateCoeffs ()
{
    double W0 = M_PI * aAfilterParams[paramID_Freq];		// use normalized frequency
	double omega = M_PI * aAfilterParams.get(paramID_Freq); // use frequency in Hz
    double kappa = omega / tan(W0);
    double omega2 = omega * omega;
    double kappa2 = kappa * kappa;
    double _1delta = 1. / (kappa*kappa + omega2 + 2.*kappa*omega); // calculate 1/delta to avoid multiple division operations
	bqA0 = omega2 * _1delta;
	bqA1 = 2. * bqA0;
	bqA2 = bqA0;
    bqB0 = 0.;
	bqB1 = (-2.*kappa2 + 2.*omega2) * _1delta;
	bqB2 = (-2.*kappa*omega + kappa2 + omega2) * _1delta;
}

void	LinkwitzRiley::HP::calculateCoeffs ()
{
    double W0 = M_PI * aAfilterParams[paramID_Freq];		// use normalized frequency
	double omega = M_PI * aAfilterParams.get(paramID_Freq); // use frequency in Hz
    double kappa = omega / tan(W0);
    double omega2 = omega * omega;
    double kappa2 = kappa * kappa;
    double _1delta = 1. / (kappa*kappa + omega2 + 2.*kappa*omega); // calculate 1/delta to avoid multiple division operations
	bqA0 = kappa2 * _1delta;
	bqA1 = -2. * bqA0;
	bqA2 = bqA0;
    bqB0 = 0.;
	bqB1 = (-2.*kappa2 + 2.*omega2) * _1delta;
	bqB2 = (-2.*kappa*omega + kappa2 + omega2) * _1delta;
}

