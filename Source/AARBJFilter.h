/*
 *	File:		AARBJFilter.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-10 by Christian Floisand
 *	Updated:	12-11-10 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AARBJFilter_h_
#define _AARBJFilter_h_

#include "AAIIRBiquad.h"


////////////////////////////////////////////////////////////////////////////////////
// RBJ coefficient subclass declarations, to be used with IIRBiquad filters
//
// Filter types:
// LP (resonant low-pass), HP (resonant high-pass),
// BP1 (band-pass, constant 0 dB peak gain), BP2 (band-pass, constant skirt gain, peak gain = Q),
// BR (notch), AP (all-pass), EQ (band-shelf equalizer), LSh (low-shelf), HSh (high-shelf)
//
// source: RBJ EQ Cookbook
////////////////////////////////////////////////////////////////////////////////////
namespace RBJ {
    
    class LP : public AADsp::AAFilter::IIRBiquad<LP> {
    public:
        void calculateCoeffs ();
    };

    class HP : public AADsp::AAFilter::IIRBiquad<HP> {
    public:
        void calculateCoeffs ();
    };
    
    // constant 0 dB gain
    class BP1 : public AADsp::AAFilter::IIRBiquad<BP1> {
    public:
        void calculateCoeffs ();
    };
    
    // peak gain = Q or BW
    class BP2 : public AADsp::AAFilter::IIRBiquad<BP2> {
    public:
        void calculateCoeffs ();
    };
    
    class BR : public AADsp::AAFilter::IIRBiquad<BR> {
    public:
        void calculateCoeffs ();
    };

    class AP : public AADsp::AAFilter::IIRBiquad<AP> {
    public:
        void calculateCoeffs ();
    };

    class EQ : public AADsp::AAFilter::IIRBiquad<EQ> {
    public:
        void calculateCoeffs ();
    };

    class LSh : public AADsp::AAFilter::IIRBiquad<LSh> {
    public:
        void calculateCoeffs ();
    };

    class HSh : public AADsp::AAFilter::IIRBiquad<HSh> {
    public:
        void calculateCoeffs ();
    };
    
} // RBJ namespace


#endif // _AARBJFilter_h_

