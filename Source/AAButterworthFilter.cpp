/*
 *	File:		AAButterworthFilter.cpp
 *
 *	Version:	1.3
 *
 *	Created:	12-10-15 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AAButterworthFilter.h"

using namespace AADsp;
using namespace AAFilter;


void	Butterworth::LP::calculateCoeffs ()
{
	double C = 1. / tan(M_PI*aAfilterParams[paramID_Freq]);
	bqA0 = 1. / (1 + SQRT2*C + C*C);
	bqA1 = 2. * bqA0;
	bqA2 = bqA0;
	bqB1 = (2. * bqA0) * (1. - C*C);
	bqB2 = bqA0 * (1. - SQRT2*C + C*C);
}

void	Butterworth::HP::calculateCoeffs ()
{
	double C = tan(M_PI*aAfilterParams[paramID_Freq]);
	bqA0 = 1. / (1 + SQRT2*C + C*C);
	bqA1 = -2. * bqA0;
	bqA2 = bqA0;
	bqB1 = (2. * bqA0) * (C*C - 1.);
	bqB2 = bqA0 * (1. - SQRT2*C + C*C);
}

void	Butterworth::BP::calculateCoeffs ()
{
	double C = 1. / tan(M_PI*aAfilterParams[paramID_Bw]);
	double D = 2. * cos(TWOPI*aAfilterParams[paramID_Freq]);
	bqA0 = 1. / (1. + C);
	bqA1 = 0.;
	bqA2 = -bqA0;
	bqB1 = -bqA0 * C * D;
	bqB2 = bqA0 * (C - 1.);
}

void	Butterworth::BR::calculateCoeffs ()
{
	double C = tan(M_PI*aAfilterParams[paramID_Bw]);
	double D = 2. * cos(TWOPI*aAfilterParams[paramID_Freq]);
	bqA0 = 1. / (1. + C);
	bqA1 = -bqA0 * D;
	bqA2 = bqA0;
	bqB1 = -bqA0 * D;
	bqB2 = bqA0 * (1 - C);
}


