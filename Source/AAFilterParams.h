/*
 *	File:		AAFilterParams.h
 *
 *	Version:	1.3
 *
 *	Created:	12-10-19 by Christian Floisand
 *	Updated:	12-12-07 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAFilterParams_h_
#define _AAFilterParams_h_

#include "AADsp.h"


// filter parameter IDs
enum
{
	paramID_Freq,       // cutoff freq in LP/HP, centre freq in BP/BR/EQ; set in Hz, converted to normalized internally
	paramID_Q,          // quality factor; only applies to BP & BR with centre frequency, and EQ
    paramID_Bw,         // bandwidth; only applies to BP & BR with centre freq, and EQ; set in Hz, converted to normalized internally
	paramID_Gain,       // overall filter gain; set in dB, converted to amplitude internally
	paramID_EqGain,     // boost/cut gain for EQ; set in dB, converted to amplitude internally
	paramID_Stages,     // number of cascaded stages of the filter, used along with filter's base order
	paramID_Sr,         // sampling rate
	
	aAfilter_numParams = 7
};

// number of parameters supported for parameter smoothing (automation)
enum
{
    aAfilter_numSmoothingParams = 5
};

namespace AADsp {

const double kParam_Defaults[aAfilter_numParams] = {
    0.01l,      // frequency, normalized
    1.l,        // Q
    0.01l,      // Bw, normalized
    1.l,        // gain, in amp
    1.l,        // eq gain, in amp
    1.l,        // number of stages
    aAsr44100,  // sampling rate
};

const double kLimits_Min[aAfilter_numParams] = {
    20.l,       // frequency, Hz
    0.1l,       // Q
    0.2l,       // Bw, Hz
    -60.l,      // gain, in dB
    -20.l,      // eq gain, in dB
    1.l,        // number of stages
    MIN_SAMPLE_RATE,	// sampling rate
};

const double kLimits_Max[aAfilter_numParams] = {
    MAX_SAMPLE_RATE,	// frequency, Hz (freq max is clamped by nyquist in normalizeFreq)
    100.l,				// Q
    480000.l,			// Bw, Hz
    20.l,				// gain, in dB
    20.l,				// eq gain, in dB
    4.l,				// number of stages
    MAX_SAMPLE_RATE,	// sampling rate
};


#pragma mark ____AAFilterParams class definition
///////////////////////////////////////////////////////////////////
// AAFilterParams class
//
// Handles parameter values for all AAFilters
///////////////////////////////////////////////////////////////////

class AAFilterParams {
    
public:
    
    AAFilterParams ()
    {
        reset();
    }
    
    AAFilterParams (const AAFilterParams& params)
    {
        for (int i = 0; i < aAfilter_numParams; ++i)
			value[i] = params[i];
    }
    
    ~AAFilterParams () {}
    
    void set (const ushort paramId, double paramValue)
    {
        paramValue = (paramValue < kLimits_Min[paramId] ? kLimits_Min[paramId] :
                      (paramValue > kLimits_Max[paramId] ? kLimits_Max[paramId] : paramValue));
        
        switch (paramId) {
            case paramID_Freq:      value[paramId] = normalizeFreq(paramValue);     break;
            case paramID_Q:         value[paramId] = paramValue;                calcBw();   break;
            case paramID_Bw:        value[paramId] = normalizeBw(paramValue);   calcQ();    break;
            case paramID_Gain:      value[paramId] = normalizeGain(paramValue);     break;
            case paramID_EqGain:    value[paramId] = normalizeEqGain(paramValue);   break;
            case paramID_Sr:
				value[paramId] = paramValue;
				nyquist = value[paramID_Sr] / 2.;
                rescaleFreq(value[paramID_Sr]/paramValue);
                rescaleBw(value[paramID_Sr]/paramValue);
                break;
            default:                value[paramId] = paramValue;                    break;
        }
    }
    
    double get (const ushort paramId) const
    {
        double val;
        switch (paramId) {
            case paramID_Freq:      val = getFrequencyHz();     break;
            case paramID_Bw:        val = getBwHz();            break;
            case paramID_Gain:      val = getGainDB();          break;
            case paramID_EqGain:    val = getEqGainDB();        break;
            default:                val = value[paramId];
        }
        return val;
    }
    
    // avoid using these two index overloading functions to set/get parameter values
    double& operator[] (ushort paramId)                { return value[paramId]; }
    const double& operator[] (ushort paramId) const    { return value[paramId]; }
	
	const AAFilterParams&	operator= (const AAFilterParams& params)
    {
		for (int i = 0; i < aAfilter_numParams; ++i)
			value[i] = params[i];
		return *this;
	}
    
    // resets all filter parameters to their default values
    void reset()
    {
		for (int i = 0; i < aAfilter_numParams; ++i)
			value[i] = kParam_Defaults[i];
		nyquist = value[paramID_Sr] / 2.;
	}
    
    // sets all smoothing parameter values (only) to 0
	// does not affect stages, sampling rate, or smoothing samples
    void clear ()
    {
		memset(value, 0, sizeof(double)*aAfilter_numParams);
    }
	
	// retrieves the bandwidth value in octaves
	double getBwOctave () const
	{
		double Q = value[paramID_Q];
		double C = sqrt(1. + 1./(4.*Q*Q));
		double f1 = value[paramID_Freq] * (C - (1./(2.*Q)));
		double f2 = value[paramID_Freq] * (C + (1./(2.*Q)));
		return (log10(f2/f1) / log10(2.));
	}
    
private:

    // parameter values
	double value[aAfilter_numParams];
	float nyquist;
    
    inline double normalizeFreq (const double freq)
    {
		// clamp to nyquist then normalize
        return (freq > nyquist ? nyquist : freq) / value[paramID_Sr];
    }
    
    inline void rescaleFreq (const double factor)
    {
        value[paramID_Freq] *= factor;
    }
    
    inline double normalizeBw (const double bw)
    {
        return bw / value[paramID_Sr];
    }
    
    inline void rescaleBw (const double factor)
    {
        value[paramID_Bw] *= factor;
    }
    
    inline double normalizeGain (const double gain)
    {
        return pow(10.l, gain/20.l);
    }
    
    inline double normalizeEqGain (const double eqgain)
    {
        return pow(10.l, eqgain/40.l); // dB/40 here to avoid needing to calculate its sqrt
    }
    
    inline double getFrequencyHz () const
    {
        return value[paramID_Freq] * value[paramID_Sr];
    }
    
    inline double getBwHz () const
    {
        return value[paramID_Bw] * value[paramID_Sr];
    }
    
    inline double getGainDB () const
    {
        return 20.l * log10(value[paramID_Gain]);
    }
    
    inline double getEqGainDB () const
    {
        return 40.l * log10(value[paramID_EqGain]);
    }
    
    inline void calcBw ()
    {
        value[paramID_Bw] = value[paramID_Freq] / value[paramID_Q];
    }
    
    inline void calcQ ()
    {
        value[paramID_Q] = value[paramID_Freq] / value[paramID_Bw];
    }

}; // AAFilterParams

}  // AADsp namespace

#pragma mark ____AAFreqResponse declaration

// AAFreqResponse
// container for holding frequency response data
//-----------------------------------------------------------------

typedef struct freq_response_ {
    float frequency;
    float magnitude;
} AAFreqResponse;

const ushort kMax_Freq_In_Response    = 4096;
const float	 kMin_Freq_In_Response    = 10.f;


#endif // _AAFilterParams_h_
