/*
 *	File:		AALinkwitzRileyFilter.h
 *
 *	Version:	1.3
 *
 *	Created:	13-01-05 by Christian Floisand
 *	Updated:	13-01-05 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AALinkwitzRileyFilter_h_
#define _AALinkwitzRileyFilter_h_

#include "AAIIRBiquad.h"


////////////////////////////////////////////////////////////////////////////////////////
// Linkwitz-Riley coefficient subclass declarations, to be used with IIRBiquad filters
// Attenuation at cutoff frequency = -6dB instead of the usual -3dB
//
// Filter types:
// LP (low-pass), HP (high-pass)
//
// source: Pirkle
/////////////////////////////////////////////////////////////////////////////////////////
namespace LinkwitzRiley {
    
    class LP : public AADsp::AAFilter::IIRBiquad<LP> {
    public:
        void calculateCoeffs ();
    };
    
    class HP : public AADsp::AAFilter::IIRBiquad<HP> {
    public:
        void calculateCoeffs ();
    };
    
} // LinkwitzRiley namespace


#endif // _AALinkwitzRileyFilter_h_