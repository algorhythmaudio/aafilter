/*
 *	File:		AAFIRMAverage.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-16 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *	@usage:--
 *		AAFilter::FIRMAverage<order> avgFilter;
 *			order is the filter order, which must be in the range 1 <= order <= 100
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAFIRMAverage_h_
#define _AAFIRMAverage_h_

#include "AAFilter.h"


namespace AADsp {
namespace AAFilter {
    
#pragma mark ____FIRMAverage filter
//////////////////////////////////////////////////////////////////////////////////////////////////
// FIRMAverage filter
//
// Simple non-recursive moving average LP filter.
// Parameter smoothing not supported.
// Includes the following types:
//      - LP up to order 100
//////////////////////////////////////////////////////////////////////////////////////////////////
template <ushort FilterOrder=1>
class FIRMAverage : public AAFilterBase {
	
	enum { maxFilterOrder_MAverage = 100 };
        
public:
        
	FIRMAverage     ()
	{
		assert(FilterOrder <= maxFilterOrder_MAverage);
		aAfilterParams[paramID_Stages] = FilterOrder;
		
		// length of filter kernel/state is 1 larger than order
		z = new float[FilterOrder+1];
		reset();

		DEBUG_REPORT_OBJECT_CREATED
		DEBUG_REPORT_OBJECT_PARAMETERS
	}
	
	~FIRMAverage    ()
	{
		delete[] z;
	}
	
	// reset, resets filter state z to 0
	//-------------------------------------------------------------------------------------------
	void	reset ()
	{
		resetFilter(FilterOrder+1);
	}
        
	// setParam
	//-------------------------------------------------------------------------------------------
	void	setParam	(const ushort paramId, double value)
	{
		assert(paramId == paramID_Gain || paramId == paramID_Sr);
		aAfilterParams.set(paramId, value);
	}
        
	// setAllParams
	//-------------------------------------------------------------------------------------------
	void    setAllParams      (const AAFilterParams& params)
	{
		// cannot set all parameters in this filter type
		assert(0);
	}
        
	// process, 32-bit
	//-------------------------------------------------------------------------------------------
	ushort	process		(float *input, float *output, const ushort inSamples)
	{
		// due to accumulating process, input buffer cannot be output buffer
		assert(input != output);
		if (aAfilterBypass)
			return 0;
            
		ushort  i, j,
				order = FilterOrder + 1;
		float   kernelCoeff = (1.f / order) * aAfilterParams[paramID_Gain]; // also scale coefficient by gain value
		outSamplesProcessed = 0;
        
		// convolution
		for (i = 0; i < inSamples; ++i) {
			z[0] = input[i];
			output[i] = aA_SSE_vAccumulate(z, order);
			
			for (j = order-1; j > 0; --j)
				z[j] = z[j-1];
			++outSamplesProcessed;
		}
		
		aA_SSE_vScale(output, kernelCoeff, output, inSamples);
		
		return outSamplesProcessed;
	}
    
#ifdef AAFILTER_USE_64bit
	// process, 64-bit
	//-------------------------------------------------------------------------------------------
	ushort	process		(double *input, double *output, const ushort inSamples)
	{
		// due to accumulating process, input buffer cannot be output buffer
		assert(input != output);
		if (aAfilterBypass)
			return 0;
            
		ushort    i, j,
				order = FilterOrder + 1;
		double  kernelCoeff = (1. / order) * aAfilterParams[paramID_Gain]; // also scale coefficient by gain value
		outSamplesProcessed = 0;
        
		// convolution
		for (i = 0; i < inSamples; ++i) {
			z[0] = input[i];
			output[i] = 0.;
                
			for (j = 0; j < order; ++j)
				output[i] += z[j];
			output[i] *= kernelCoeff;
			
			--j;
			for (; j > 0; --j)
				z[j] = z[j-1];
			++outSamplesProcessed;
		}
		
		return outSamplesProcessed;
	}
#endif
    
private:
	
	// calcFreqResponseMagnitudeAt
	//-------------------------------------------------------------------------------------------
	inline float calcFreqResponseMagnitudeAt (const float w)
	{
		ushort order = FilterOrder + 1;	// account for first term in difference equation
		float kernelCoeff = 1.f / order;
		float cosX = 0.f, sinX = 0.f;
		
		for (int i = 0; i < order; ++i) {
			cosX += cosf(i*w);
			sinX += sinf(i*w);
		}
		
		cosX *= kernelCoeff;
		sinX *= kernelCoeff;
		cosX *= cosX;	// cosX^2
		sinX *= sinX;	// sinX^2
		
		return sqrtf(cosX+sinX);
	}
        
};	// FIRMAverage
    
} } // AAFilter namespace // AADsp namespace


#endif // _AAFIRMAverage_h_

