/*
 *	File:		AAButterworthFilter.h
 *
 *	Version:	1.3
 *
 *	Created:	12-10-15 by Christian Floisand
 *	Updated:	12-11-10 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 */

#ifndef _AAButterworthFilter_h_
#define _AAButterworthFilter_h_

#include "AAIIRBiquad.h"


////////////////////////////////////////////////////////////////////////////////////
// Butterworth coefficient subclass declarations, to be used with IIRBiquad filters
//
// LP, HP, BP, BR types
// source: Dodge & Jerse, pg. 215 - 217
////////////////////////////////////////////////////////////////////////////////////
namespace Butterworth {
    
    class LP : public AADsp::AAFilter::IIRBiquad<LP> {
    public:
        void calculateCoeffs ();
    };
    
    class HP : public AADsp::AAFilter::IIRBiquad<HP> {
    public:
        void calculateCoeffs ();
    };

    class BP : public AADsp::AAFilter::IIRBiquad<BP> {
    public:
        void calculateCoeffs ();
    };

    class BR : public AADsp::AAFilter::IIRBiquad<BR> {
    public:
        void calculateCoeffs ();
    };
    
} // Butterworth namespace


#endif // _AAButterworthFilter_h_
