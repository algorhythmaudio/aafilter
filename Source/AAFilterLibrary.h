/*
 *	File:		AAFilterLibrary.h
 *
 *	Version:	1.3
 *
 *	Created:	12-11-15 by Christian Floisand
 *	Updated:	13-01-05 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *  Include this to get all AA filters.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */

#ifndef _AAFilterLibrary_h_
#define _AAFilterLibrary_h_

#include "AAFilter.h"
#include "AAFilterParams.h"
#include "AAIIRBiquad.h"
#include "AAIIRSimple.h"
#include "AAButterworthFilter.h"
#include "AAParametricEQ.h"
#include "AARBJFilter.h"
#include "AALinkwitzRileyFilter.h"
#include "AAFIRMAverage.h"
#include "AAFIRDesign.h"
#include "AAFIRPolyphase.h"

#endif // _AAFilterLibrary_h_
