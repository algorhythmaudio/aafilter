/*
 *	File:		AARBJFilter.cpp
 *
 *	Version:	1.3
 *
 *	Created:	12-11-10 by Christian Floisand
 *	Updated:	13-01-12 by Christian Floisand
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include "AARBJFilter.h"

using namespace AADsp;
using namespace AAFilter;


void	RBJ::LP::calculateCoeffs ()
{
    double W0 = TWOPI * aAfilterParams[paramID_Freq];		// freq already normalized
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 = (1. - cosW0) / 2.;
	bqA1 = 1. - cosW0;
	bqA2 = bqA0;
    bqB0 = 1. + alpha;
	bqB1 = -2. * cosW0;
	bqB2 = 1. - alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::HP::calculateCoeffs ()
{
    double W0 = TWOPI * aAfilterParams[paramID_Freq];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 = (1. + cosW0) / 2.;
	bqA1 = -(1. + cosW0);
	bqA2 = bqA0;
    bqB0 = 1 + alpha;
	bqB1 = -2. * cosW0;
	bqB2 = 1. - alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::BP1::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 = alpha;
	bqA1 = 0.;
	bqA2 = -bqA0;
    bqB0 = 1. + alpha;
	bqB1 = -2. * cosW0;
	bqB2 = 1. - alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::BP2::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 = aAfilterParams[paramID_Q] * alpha;
	bqA1 = 0.;
	bqA2 = -bqA0;
    bqB0 = 1. + alpha;
	bqB1 = -2. * cosW0;
	bqB2 = 1. - alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::BR::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 = 1.;
	bqA1 = -2. * cosW0;
	bqA2 = 1.;
    bqB0 = 1. + alpha;
	bqB1 = bqA1;
	bqB2 = 1. - alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::AP::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 = 1. - alpha;
	bqA1 = -2. * cosW0;
	bqA2 = 1. + alpha;
    bqB0 = bqA2;
	bqB1 = bqA1;
	bqB2 = bqA0;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::EQ::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
	double sinW0 = sin(W0);
    double A = aAfilterParams[paramID_EqGain];    // EqGain already normalized
	double alpha = sinW0 * sinh(0.346573590279973*aAfilterParams.getBwOctave()*W0/sinW0);
	bqA0 = 1. + alpha*A;
	bqA1 = -2. * cos(W0);
	bqA2 = 1. - alpha*A;
    bqB0 = 1. + alpha/A;
	bqB1 = bqA1;
	bqB2 = 1. - alpha/A;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::LSh::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
    double A = aAfilterParams[paramID_EqGain];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 =      A * ( (A+1.) - (A-1.)*cosW0 + 2.*sqrt(A)*alpha );
	bqA1 = 2. * A * ( (A-1.) - (A+1.)*cosW0 );
	bqA2 =      A * ( (A+1.) - (A-1.)*cosW0 - 2.*sqrt(A)*alpha );
    bqB0 =            (A+1.) + (A-1.)*cosW0 + 2.*sqrt(A)*alpha;
	bqB1 =    -2. * ( (A-1.) + (A+1.)*cosW0 );
	bqB2 =            (A+1.) + (A-1.)*cosW0 - 2.*sqrt(A)*alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}

void	RBJ::HSh::calculateCoeffs ()
{
	double W0 = TWOPI * aAfilterParams[paramID_Freq];
    double A = aAfilterParams[paramID_EqGain];
	double alpha = sin(W0) / (2.*aAfilterParams[paramID_Q]);
    double cosW0 = cos(W0);
	bqA0 =      A * ( (A+1.) + (A-1.)*cosW0 + 2.*sqrt(A)*alpha );
	bqA1 = -2. * A * ( (A-1.) + (A+1.)*cosW0 );
	bqA2 =      A * ( (A+1.) + (A-1.)*cosW0 - 2.*sqrt(A)*alpha );
    bqB0 =            (A+1.) - (A-1.)*cosW0 + 2.*sqrt(A)*alpha;
	bqB1 =     2. * ( (A-1.) - (A+1.)*cosW0 );
	bqB2 =            (A+1.) - (A-1.)*cosW0 - 2.*sqrt(A)*alpha;
    
    bqA0 /= bqB0;
    bqA1 /= bqB0;
    bqA2 /= bqB0;
    bqB1 /= bqB0;
    bqB2 /= bqB0;
}
