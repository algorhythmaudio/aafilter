/*
 *	File:		AAFilter.cpp
 *	
 *	Version:	1.3
 * 
 *	Created:	12-09-23 by Christian Floisand
 *	Updated:	12-12-07 by Christian Floisand
 *	
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 * 
 *  Class definition of base filter interface class.
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include <iostream>
#include "AAFilter.h"

using namespace AADsp;
using namespace AAFilter;


#pragma mark ____AAFilterBase definitions

//
// AAFilterBase::AAFilterBase constructor
//-------------------------------------------------------------------------------------------
AAFilterBase::AAFilterBase () : aAfilterBypass(false), z(NULL) {}

//
// AAFilterBase::~AAFilterBase
//-------------------------------------------------------------------------------------------
AAFilterBase::~AAFilterBase () {}

//
// AAFilterBase::setBypass
//-------------------------------------------------------------------------------------------
void AAFilterBase::setBypass  (const bool bypass)
{
    if (bypass == aAfilterBypass)
        return;
    
    aAfilterBypass = bypass;
    if (aAfilterBypass)
        reset();
}

//
// AAFilterBase::getParam
//-------------------------------------------------------------------------------------------
double AAFilterBase::getParam  (const ushort paramId) const
{
    assert(paramId >= 0 && paramId < aAfilter_numParams);
    return aAfilterParams.get(paramId);
}

//
// AAFilterBase::resetFilter
// resets filter state z to 0
//-------------------------------------------------------------------------------------------
void AAFilterBase::resetFilter (const ushort length)
{
    memset(z, 0, sizeof(float)*length);
}

//
// AAFilterBase::getFrequencyResponse
// calculates the magnitude response at frequencies equally space out between MIN_FREQUENCY_IN_RESPONSE
// to Nyquist according to numFrequencies
// returns 1 on success, 0 otherwise
//-------------------------------------------------------------------------------------------
ushort AAFilterBase::getFrequencyResponse   (AAFreqResponse* const freqResponseArray, const ushort numFrequencies)
{
    if (numFrequencies > kMax_Freq_In_Response || freqResponseArray == NULL)
        return 0;
    
    float nyquistFreq = aAfilterParams[paramID_Sr] / 2.f;
    float freqIncr = (nyquistFreq - kMin_Freq_In_Response) / (numFrequencies-1);   // -1 to allow for first point in array
    freqIncr *= (TWOPI/aAfilterParams[paramID_Sr]);                               // normalize
    float curFreq = kMin_Freq_In_Response * TWOPI / aAfilterParams[paramID_Sr];   // normalize
    
    for (int i = 0; i < numFrequencies; ++i) {
        freqResponseArray[i].frequency = curFreq;
        freqResponseArray[i].magnitude = calcFreqResponseMagnitudeAt(curFreq);
        curFreq += freqIncr;
    }
    
    return 1;
}

//
// AAFilterBase::printParameters
// prints all the parameters out to the console
// mostly for debugging purposes
//-------------------------------------------------------------------------------------------
void AAFilterBase::printParameters() const
{
    std::cout << "Frequency (Hz) \t\t Q \t\t Bandwidth (Hz) \t\t Gain (dB) \t\t " <<
                "EqGain (dB) \t\t Order \t\t Sample Rate \t\t Smoothing samples" << std::endl;
    for (int i = 0; i < aAfilter_numParams; ++i) {
        std::cout << aAfilterParams.get(i) << "\t\t\t\t";
    }
    std::cout << std::endl;
}


#pragma mark ____AASmoothedFilter definitions

//
// AASmoothedFilter::AASmoothedFilter default constructor
//-------------------------------------------------------------------------------------------
AASmoothedFilter::AASmoothedFilter () : aAfilterParamSmoothing(true),
                                            numSmoothingSamples(kNumSmoothingSamples_Default)
{
    paramSmoothingFactor = 1. / numSmoothingSamples;
    /*
     make sure any filter type that inherits from AASmoothedFilter calls its reset() function
     in the constructor that clears the smoothing increment parameters: aAfilterSmoothingIncr.clear()
     */
}

//
// AASmoothedFilter::AASmoothedFilter parameter constructor
//-------------------------------------------------------------------------------------------
AASmoothedFilter::AASmoothedFilter (const bool pSmoothing) : aAfilterParamSmoothing(pSmoothing),
                                                                numSmoothingSamples(kNumSmoothingSamples_Default)
{
    paramSmoothingFactor = 1. / numSmoothingSamples;
}

//
// AASmoothedFilter::AASmoothedFilter destructor
//-------------------------------------------------------------------------------------------
AASmoothedFilter::~AASmoothedFilter ()
{
}

//
// AASmoothedFilter::setNumParamSmoothingSamples
// sets the number of samples over which to peform parameter smoothing
//-------------------------------------------------------------------------------------------
void	AASmoothedFilter::setNumParamSmoothingSamples	(const uint nSamples)
{
    numSmoothingSamples = nSamples;
    numSmoothingSamples = (numSmoothingSamples < kNumSmoothingSamples_Min ? kNumSmoothingSamples_Min :
                           (numSmoothingSamples > kNumSmoothingSamples_Max ? kNumSmoothingSamples_Max : numSmoothingSamples));
    paramSmoothingFactor = 1. / numSmoothingSamples;
}

//
// AASmoothedFilter::setParamSmoothing
// turns parameter smoothing of the filer on/off according to pSmoothing
//-------------------------------------------------------------------------------------------
void	AASmoothedFilter::setParamSmoothing		(const bool pSmoothing)
{
    if (pSmoothing == aAfilterParamSmoothing)
        return;
    
    aAfilterParamSmoothing = pSmoothing;
    aAfilterSmoothingIncr.clear();
    if (!aAfilterParamSmoothing)
        numSmoothingSamplesRemaining = -1;
}

//
// AASmoothedFilter::prepareParamSmoothing, all parameters change
// calculates the interpolating increment amount for use in parameter smoothing
//-------------------------------------------------------------------------------------------
void	AASmoothedFilter::prepareParamSmoothing	(const AAFilterParams& filterParams, const AAFilterParams& newParams)
{
    numSmoothingSamplesRemaining = numSmoothingSamples;
    
    double val;
    
    for (int i = 0; i < aAfilter_numSmoothingParams; ++i) {
        val = newParams[i];
        val = (val < kLimits_Min[i] ? kLimits_Min[i] : (val > kLimits_Max[i] ? kLimits_Max[i] : val));
        aAfilterSmoothingIncr[i] = (val - filterParams[i]) * paramSmoothingFactor;
    }
}

//
// AASmoothedFilter::prepareParamSmoothing, single parameter change
// calculates the interpolating increment amount for use in parameter smoothing
//-------------------------------------------------------------------------------------------
void	AASmoothedFilter::prepareParamSmoothing	(const AAFilterParams& filterParams, const ushort id, double val)
{
    assert(id >= 0 && id < aAfilter_numSmoothingParams);
    numSmoothingSamplesRemaining = numSmoothingSamples;
    
    // force bounds
    val = (val < kLimits_Min[id] ? kLimits_Min[id] : (val > kLimits_Max[id] ? kLimits_Max[id] : val));
    if (id == paramID_Freq || id == paramID_Bw)
        val = val / filterParams[paramID_Sr];
    
    aAfilterSmoothingIncr[id] = ( val - filterParams[id]) * paramSmoothingFactor;
}

