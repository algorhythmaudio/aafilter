
	Development Notes on AAFilter class
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Update (CF) 01/31/13:
    Due to the gain compensation required in interpolation, gain cannot be removed from the interpolator function.  It shouldn't 
    be required in decimate though.

Update (CF) 01/26/13:
    Removed gain parameter from the polyphase interpolator and decimator functions.

Update (CF) 01/14/13:
    Removed aA_fast trig functions, because after more thorough testing it appears as though the computational savings are very slight 
    and some times even exceed that of sin/cos (especially in a Release build).  Testing was done with parameter automation on 
    an RBJ::LP filter.

Update (CF) 01/13/13:
    Fixed a bug where a reference to the delay line "z" allocated the incorrect amount of space in a 64-bit host due to the pointer 
    size being different, causing memory corruption.

Update (CF) 01/12/13:
    Replaced occurrences of sin() and cos() with fast approximation functions aA_fastSin/aA_fastCos.
    
    Added additional preprocessor defines in AAFIRDesign to enable which filters to use.

Update (CF) 01/09/13:
    Added compiler define AAFILTER_USE_64bit that enables 64-bit processing functions.

Update (CF) 01/07/13:
    Added Linkwitz-Riley filter (LP & HP types), implemented by IIRBiquad.  
    
    Some of the FIR filters have been optimized with intrinsics for vector calculation.

Update (CF) 12/07/12:
    Implemented fairly significant optimization of the overall filter class by separating the parameter smoothing-specific 
    elements into its own class.  Any filter type that needs to implement this feature needs to inherit from AASmoothedFilter, which 
    contains all the methods and variables necessary.  As a result, filters that do not need to implement this feature will not 
    be bogged down with all the extra, unused code, resulting in a reduced footprint and more efficient initialization.

Update (CF) 12/05/12:
    Fixed a bug where I was passing in the wrong value for "size" in the memcpy instructions in FIRDesign for the getCoeffs() 
    functions.  This may have been the cause of the bugs related to the std library "fill" and "copy" functions as well.  Might 
    try reimplementing those to verify.

Update (CF) 12/04/12:
    Replaced most occurrences of "int" or "uint" with "short" and "ushort".  
    Replaced all occurrences of "memset" or "memcpy" with std library functions "fill" and "copy."

Update (CF) 12/03/12:
    Fixed a bug in the moving average filter that failed to account for the first term (x[0]) in the difference equation.

Update (CF) 12/02/12:
    Fixed a bug that occurred when automating the Q value individually in all filters that made it go down to 0.  
    
    All process() functions now return a value equal to the number of output samples that were processed in the block.  
    Most of the time this should and will equal to the number of input samples, but in the case of FIR filters it should 
    equal to inSamples+size of the kernel due to convolution.  And in the case of the interpolation and decimation functions 
    in FIRPolyphase, the outSamplesProcessed value that the functions will return should be the size of the output buffer 
    as a result of the upsampling/downsampling factor.  This has yet to be implemented.

Update (CF) 12/01/12:
    Fixed a bug in interpolate() that didn't correctly apply the prototype polyphase filter, and resulted in "staircasing" of the 
    upsampled waveform.  This was fixed by flipping the filter kernel (which is implemented just by reversing the indexing).

Update (CF) 11/30/12:
    Optimized FIRDesign processing to take advantage of symmetry of the filter kernel.  The same should supposedly be possible with 
    the polyphase design, but upon initial testing it instead added time to the execution.  This is likely due to the more complex 
    indexing required due to the organization of the filter's kernel into a matrix and sub filters.

Update (CF) 11/29/12:
    Fixed issue with the FIRPolyphase interpolator and decimator.  The decimator actually does not use a delay line, but instead 
    operates on the input buffer directly.  
    
    Still need to handle the offset in the audio due to the delay in the interpolator (possible with a delay buffer using 
    overlap-add?).

Update (CF) 11/28/12:
    Added denormal prevention to the IIR filters (IIRBiquad & IIRSimple).

Update (CF) 11/27/12:
    Fixed a bug in the constructor of FIRPolyphase that didn't initialize the filter state (z buffer) to 0.

Update (CF) 11/26/12:
    Fixed a bug in the FIRPolyphase setParam method that failed to set the proper parameter.  
    The method now sets the right parameter by using the 'this' pointer.

Update (CF) 11/25/12:
    Added interpolate and decimate templated methods to FIRPolyphase for use in resampling.  

Update (CF) 11/24/12:
    FIRPolyphase is working, albeit in a preliminary stage.  Testing remains to see what the savings on performance it affords.
    Subsequently it will be adapted to be a upsampling/downsampling filter by including that process within the filter by the 
    commutative property.  

Update (CF) 11/19/12:
    Made the function getOrder() virtual, since most filter implementations (with a base order of 1 or whose order increases by a 
    factor of 1) simply return the aAfilterParams[paramID_Stages] parameter.  The IIRBiquad implementation needs to multiply it 
    by its base order of 2.
    
    Made the function reset() virtual, since the FIR filters allow a much greater filter order and as such needs to accomodate for 
    the increased length of the filter's state buffer z.

Udpate (CF) 11/18/12:
    Added the window-sinc FIR design.  
    Fixed some errors in the calculations for the Hamming window design that included incorrect value for the filter order as it 
    was being used in the loop bounds.  
    
    This needs to be checked for the frequency sampling method as well.

Update (CF) 11/17/12:
    Implemented the magnitude frequency response calculations for the new FIR filters.  
    Fixed a bug in the processing of FIRMAverage that incorrectly calculated the filter state z by out-of-bounds indexing.  
    Improved namespace protection in all files.  
    Wrapped the outputting of coefficient variables for the FIR design filters inside a preprocessor block (AADebug__).

Update/Soved (CF) 11/16/12:
    The error occurring as a result of even-numbered filter orders > 2 in FIRMAverage has been fixed.  It was likely due to 
    the fact that the same buffer was being used for input and output, and since this is an accumulating process, a memory 
    error resulted in EXEC_BAD_ACCESS.  
    
    Finalized design filters, and added documentation.  It is important to note (although an assertion is in place) that 
    input != output in these FIR filters, and this was resulting in unexpected behavior alluded to earlier.

Update (CF) 11/16/12:
    Added AAFIRDesign filter, which allows the design of a FIR filter of type LP or HP according to design methods.  So far, 
    only the frequency sampling method is supported in which an impulse response is passed into the filter along with the 
    desired order and sample rate from which to calculate the coefficients.  
    
    Note that the design method is not foolproof, and that "wrong" values for the impulse response and filter order can result 
    in unpredictable behavior.  
    
    These filters do not operate based on parameter values for frequency, Q, or bandwidth, so these parameters are not modifiable.  
    The gain parameter is still valid, though it can also result in unpredictable behavior especially if > 0 (in dB).

Problem (CF) 11/16/12:
    A very bizarre error is occuring when the AAFIRMAverage filter is set to an even-numbered order > 2 (i.e. 4, 6, 8, etc.) that 
    causes execution to terminate during std::cout or printf calls to ostream.  There appears to be no problem at all with 
    the filter's code, as it applies the effect correctly.

Update (CF) 11/15/12:
    Added AAFIRMAverage, a simple moving average non-recursive filter.  This filter does NOT calculate based 
    on a given frequency, and only includes LP type.  However, it can go up to a max order of 50. 
    
    Fixed potential issues with circular include errors.  
    Added the AAFilterLibrary.h file -- this is the only file that needs to be included to get all filters.

Update (CF) 11/14/12:
    Added the AAIIRSimple implementation, simple, recursive, first-order LP and HP types.  
    New filter types added include:
    - Simple::LP & Simple::HP
    - DJBasic::LP & DJBasic::HP

Update (CF) 11/13/12:
    Made AAFilterBase a proper abstract base class with overloaded process functions so that we can declare a pointer to it that 
    can then be used to reference instantiated filter objects.  
    
    Fixed an issue where parameter smoothing would allow parameters to go out of bounds.
    Fixed an issue where the getEqGainDB method in AAFilterParams returned the wrong value;

Update (CF) 11/12/12:
    Merged optimization branch into master branch.
    
    Version upgraded to 1.3
    
Update (CF) 11/11/12:
    Added support for calculating the frequency response of the biquad filters.
    
Update (CF) 11/10/12:
    Some major changes to the optimized filter class.  
    
    - Consolidated filters into IIR biquad implementations.
    - Generic/basic IIR filters have been removed, but will be re-implemented as IIR simple filters in the future.  
      (This was to optimize out the different implementation equation, since the basic IIR LP & HP filters were first-order.)
    - Added support for changing a single parameter directly throught the filter interface.
    - Added RBJ filters.
    
    Templated classes are fully implemented so that all filters that utilize the biquad difference equation go through 
    AAIIRBiquad, with the coefficients handled by each particular filter's class/file.

Update (CF) 10/20/12:
    Got rid of the variable paramSmoothingSamples and made the FilterParam represent this value instead of the duration 
    over which parameter smoothing should take place. This could easily be abstracted outside the filter class, and 
    the calculations were reduntant to get the required samples.

Update (CF) 10/20/12:
    Experimenting around with better design and efficiency.  The derived classes of Butterworth and IIR use templates
    to subdefine their filter types.  Parametric EQ is left alone because it has only one type.  
    
    No significant performance enhancement was noticed in the processing, but the code is much cleaner and less 
    repetition of lines/blocks brings the code size down significantly.
    
    Committed to branch "optimizations".

Update (CF) 10/19/12 - version 1.2 :
    Some fairly major changes have been made, including moderate redesign of the class.  This was mainly to accomodate 
    parameter smoothing, which has been successfully implemented and tested, but also paves the way for increased 
    efficiency and streamlined code.
    
    Filter parameters are now their own class, operating partly outside of direct access to the filter class itself.  
    This was essential so that multiple parameters can be automated at the same time in an efficient way.  The 
    AAFilterParams.h file does not need to be explicitly included.
    
    Stereo processing is no longer supported.  Instead, use de-interleaved audio to process each channel with 
    a different filter.  
    
    Version upgraded to 1.2

Update (CF) 10/15/12 - version 1.1 :
    Implemented the AAParametricEQ class, a parametric equalizer. Equation coefficients are from the RBJ EQ Cookbook.  
    
    Reorganized entire AAFilter class project into separate .h/.cpp files for each filter type.  That way, we only 
    need to include the specific filters to be used.  The AAFilter namespace is now handled in a way that the user 
    does not need to prepend class object defines with the scope of AAFilter.  See header files for usage.
    
    Version upgraded to 1.1
    
    Fixed a bug in setFrequency that didn't scale the initial conditional comparison to the incoming freq argument.

Update (CF) 10/14/12:
    Added AADsp.h file to filter class.

Update (CF) 10/14/12:
	Added copy constructors and operator overloads to facilitate handling of multiple filter objects.

Update (CF) 10/12/12:
	Request for a process function that takes and processes a single sample was to be implemented, but this can 
	be achieved simply by calling the existing process function with 'frames' set to 1.
		e.g. filter.process(&buffer[i], &buffer[i], 1);
	
	Optimized internal calculations by storing the filter frequency as a normalized value (of range 0 - 0.5; 
	with 0.5 = Nyquist).  This prevents the need to divide by the sample rate in the coefficients' calculations.

Update (CF) 10/10/12:
	Added the following methods to the class:
		- setChanMode
	Added namespace to protect against name collisions with certain global variables (e.g. TWOPI).  
	Expanded the processing functions to accept an output buffer to place the filtering in, useful 
	for parallel processing.
	Added .gitignore file to repository.

Update (CF) 10/06/12 - version 1.0 :
	Added version control.  Pushed to repository.

Update (CF) 10/1/2012:
	Version 1.0 completed.
	All basic functionality in place for both filter classes (Butterworth and generic IIR), and 
	verified working correctly (albeit with limited extensive testing).
	
Update (CF) 09/31/2012:
	The automating of parameters in the generic IIR filter class does not seem to result in the same 
	clicks/pops that occur in the Butterworth filter.  Automating seems generally smoother, but I don't 
	know why.  Also, automating (simply turning on/off) the bypass does not result in any clicks and 
	seems to work well.
	
Problem (CF) 09/30/2012:
	Automating parameters such as frequency, Q, filter type, and order results in clicks/pops occurring between 
	the processed blocks.  I believe this has to do with the slight difference in amplitude as a result of the 
	filtering using different parameters, and this causes an abrupt change (albeit rather subtle for the most part) 
	in the amplitude.  Some kind of signal balancing might solve this?
	
